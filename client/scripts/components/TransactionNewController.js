angular
  .module('app').controller('TransactionNewController', TransactionNewController);
function TransactionNewController($scope, $rootScope,$stateParams, $state, toastr,  Utils,Transaction) {  //init objeck
  // $scope.modeRole = function(mode){
  //   console.log('mode', mode)
  //   $scope.mode = mode
  // }
  $scope.isEdit = false
  $scope.isPrint = false;
  $scope.currentUser = $rootScope.currentUser
  $scope.transaction={}
  $scope.jenisBarangList = ['Konsumsi', 'Pembersih']
 
 
  $scope.mode = 'new'
  //console.log('$state.current', $state.current)
  if($stateParams.id){
    Transaction.findById({
      id: $stateParams.id, filter: {
        include: []
      }
    }, function (response) {
      
      console.log('trabs', response)
      if (response.tgl_transaksi) response.tgl_transaksi = new Date(response.tgl_transaksi), 'yyyy-MM-dd';
       console.log('$scope.transaction', $scope.transaction)
       $scope.transaction = response
      
    })
  }
  $scope.validateKeyCharOnly = function (val, el) {
    $scope[val][el] = Utils.validateKeyCharOnly($scope[val][el]);
  }

  $scope.validateNumber = function (val, data, key, max) {
    $scope[data][key] = Utils.validateNumber(val, max)
  }

  $scope.validateKeyNumber = function (data, element) {
    $scope[data][element] = Utils.validateKeyNumber($scope[data][element])
  }

  $scope.disableNumberSymbol = function (event) {
    if (!Utils.disableNumberSymbol(event.keyCode))
      event.preventDefault()
  }

  $scope.sum = function(items,prop){
    return items.reduce( function(a,b){
        return a + b[prop];
    }, 0);
  };


  $scope.dateNow = moment(new Date()).format('YYYY-MM-DD')
  ///$scope.supp_buyers_types = ["Pemasok Dominan", "Pembeli Dominan"]
  $scope.members = []
  $scope.developer_group = []
 
  $scope.currentController = 'new'
  $scope.go = function go(url) {
    $location.path(url);
  }
  $scope.cancel = function cancel() {
    $state.go('transaction');
  }

  //function saveAll
 

 

  

  
  //$scope.initPrint()
  $scope.saveAll = function saveAll() {
    if($stateParams.id){
      $scope.transaction.updated_at = new Date()
      $scope.transaction.$save(()=>{
        $state.go('transaction');
      },(err)=>{
        toastr.warning('Terjadi kesalahan....'); 
      })
    }else{
      $scope.transaction.created_at = new Date()
      $scope.transaction.updated_at = new Date()
      //console.log('customer', $scope.customer);
    
      //return
      Transaction.create($scope.transaction, function onSuccess(response) {
        
        $state.go('transaction');
          //$state.go('component.jobsheet');
      }, function onFailed(err) { toastr.warning('Terjadi kesalahan....'); 
        
      })
      // } catch(err){
      //   console.log('error:', err)
      // }
    }
   
  }

  $scope.validateNumber = function validateNumber(val, max, min, positveOnly) {
    !positveOnly ? positveOnly = true : positveOnly = positveOnly;
    if (val < 0 && positveOnly) return val * -1
    if (max) if (val > max) return max
    if (min) if (val < min) return min
    return val
  }


 
}
