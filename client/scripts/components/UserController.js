angular
.module('app').controller('UserController', UserController);
angular
.module('app').controller('UserEditController', UserEditController);
angular
.module('app').controller('UserNewController', UserNewController);

function UserController($q,$scope, $rootScope, $state,Cabang, MstUserRole, Role, User,  $location, toastr, $timeout, $stateParams, Utils ) {
    $scope.addUser = false
    $scope.invalidPassword = false
    $scope.mode = 'entry'

    if($stateParams && $stateParams.mode) $scope.mode = $stateParams.mode

    $scope.user_view = User.find(); 
    $scope.filter = {}
    $scope.filter.user = {}; 
    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    $scope.searchTextEXS = '';
    $scope.currentUser = $rootScope.currentUser;
    // if( $scope.currentUser.fk_cabang == '700') $scope.addUser = true
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    // Filter list base on parameter 
    $scope.Bfilter = function Bfilter(argument) {
        console.log("Filter")
        $scope.getPagedDataAsync();
    }
    $scope.Reset = function Reset(){
        $scope.filter.user = {};
    }

    // Page data 
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            User.find({}, function onSuccess(rowscount){
                //console.log("rowscount:", rowscount.length)
                $scope.totalServerItems = rowscount.length;
            }, function onFailed(err){console.log('gagal find data:', err)});
            if (searchText) {
                var ft = searchText.toLowerCase();
                $scope.searchTextEXS = ft;
                data = User.find({filter:{
                    // include : ["cabang"], 
                    where: 
                        {or: [{code:{like:'%'+ft+'%'}},
                        {name:{like:'%'+ft+'%'}}]},
                    order: 'name', offset: offset, limit: pageSize}},function onSuccess(list){
                    //     list.forEach(function(data_user){
                    //     if(data_user.STATUS == 'A'){
                    //         data_user.STATUS = 'Active'
                    //     }else{
                    //         data_user.STATUS = 'Inactive'
                    //     }
                    // })
                    $scope.listlength = list.length;
                }, function onFailed(err){console.log('gagal find data:', err)});
            } else {
                var filterParams = [];
                console.log($scope.filter.user.branch)
                if ($scope.filter.user.branch){
                    //filterParams.push({"FID_BRANCH": $scope.filter.user.branch.ID});
                    // filterParams.push({"fk_cabang": $scope.filter.user.branch.ID});
                } 
                if ($scope.filter.user.user_name){
                    filterParams.push({"username": $scope.filter.user.user_name.username});
                }
                if ($scope.filter.user.fullname){
                    filterParams.push({"NAME": $scope.filter.user.fullname.NAME});
                }
                console.log("Params:", filterParams)
                data = User.find({
                    filter:{
                        // include :["cabang"], 
                        where:{
                            and:
                                filterParams, 
                        }, 
                        order:'ID', 
                            offset : offset, 
                            limit : pageSize
                    }
                }, function onSuccess(list){
                    // list.forEach(function(data_user){
                    //     if(data_user.STATUS == 'A'){
                    //         data_user.STATUS = 'Active'
                    //     }else{
                    //         data_user.STATUS = 'Inactive'
                    //     }
                    // })
                    console.log("list", list)
                })
            }
            $scope.setPagingData(data,page,pageSize);
        }, 100);
    };
    // $scope.exportData = function(searchText){
    //     var totrows = $scope.listlength;
    //     var pageSize = 1000;
    //     var totpages = Math.ceil(totrows / pageSize);
    //     var custacqui = [];
    //     var promises = [];
    //     var ft = $scope.searchTextEXS;
    //     var filterParams = {};

    //     function callUpdate(x, promises){
    //         var d = $q.defer();
    //         Region.find({filter:{where: {or: [{code:{like:'%'+ft+'%'}},{name:{like:'%'+ft+'%'}}]},offset: offset, order: 'id DESC'}},function onSuccess(list){
    //             list.forEach(function(sales){
    //                 custacqui.push({
    //                     Code:sales.code,
    //                     Name:sales.name,
    //                 });
    //             });
    //             d.resolve();
    //         });
    //         promises.push(d.promise);
    //     }

    //     for(var i = 0; i < totpages; i++){
    //         var offset = i * pageSize;
    //         callUpdate({offset: offset}, promises);
    //     }

    //     $q.all(promises).then(function(_data){
    //         //console.log('jumlah rows:', custacqui, _data);
    //         alasql('SELECT * INTO XLSX("Region.xlsx", {headers: true}) FROM ? ',[custacqui]);
    //     });
    // }
     $scope.exportData = function(searchText){
        toastr.info('Mengexport...')
        var totrows = $scope.totalServerItems;
        $scope.pageSize = 2000;
        $scope.totpages = Math.ceil($scope.totalServerItems / $scope.pageSize);
        var custacqui = [];
        var promises = [];
        var ft = $scope.searchTextEXS;
        //let filterParams = {};

        function callUpdate(x, promises,it){
            //alert('ok')
            let d = $q.defer();
             let filterParams = [];
               
                User.find({filter:{include : ['cabang',{'mstrole': 'mstroles'}],
                    order: 'name', 
                    offset : x.offset, 
                    limit : $scope.pageSize
                 }},function onSuccess(list){
                    //     list.forEach(function(data_user){
                    //     if(data_user.STATUS == 'A'){
                    //         data_user.STATUS = 'Active'
                    //     }else{
                    //         data_user.STATUS = 'Inactive'
                    //     }
                    // })
                    list.forEach((user,i)=>{
                         custacqui.push({
                                'ID/Username' : user.ID,
                                'Nama' : user.NAME ,
                                // 'Cabang' : user.cabang ? user.cabang.BRANCH_NAME : '',
                                'Role' : user.mstrole[0] ? user.mstrole[0].mstroles ? user.mstrole[0].mstroles.ROLE_NAME : '' : '',
                            });
                            if(i == list.length - 1){//0 == 1-1
                             d.resolve();
                            }                       
                    })
                }, function onFailed(err){console.log('gagal find data:', err)});
            
            promises.push(d.promise);
        }

        for(let i = 0; i < $scope.totpages; i++){
            setTimeout(()=>{
                $scope.offset = i * $scope.pageSize;
                console.log(`$scope.offset (${$scope.offset}), i ${i}`)
                callUpdate({offset: $scope.offset}, promises, i);
            },100)
            
        }
         for(let i = 0; i < $scope.totpages; i++){
            setTimeout(()=>{
                toastr.info('Mengexport...', {timeOut: 100})
                toastr.remove()
            },1000 * i )
        }
        
        setTimeout(()=>{


        $q.all(promises).then(function(_data){
            //console.log('jumlah rows:', custacqui, _data);
            custacqui = custacqui.sort((a,b)=>{
                return b.ID - a.ID
            })
            alasql('SELECT * INTO XLSX("User.xlsx", {headers: true}) FROM ? ',[custacqui]);
        });
         },5000)
    }
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
            $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, 500);
    }, true);

    $scope.go = function go(url){
        $location.path(url);
    }

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        //console.log('view ID:', id, index);
        $location.path('/component/user/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].ID;
        $location.path('/component/user/'+id+'/edit');
    };
    $scope.viewRowIndex1 = function viewRowIndex1(row, index){
        var id = $scope.myData[index].ID;
        $location.path('/component/user/'+$scope.mode+'/'+id);
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.ID;
        var deleteUser = row.entity;
        console.log("name :", row.entity.NAME); 
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            var message;
            MstUserRole.find({
                filter:{
                  where:{IDF_USER:row.entity.ID}
                }
            }, function onSuccess(result){
                for(data of result){
                    console.log("acl:", data)
                    MstUserRole.deleteById({id : data.IDF_USER});
                }  
            }); 
            User.deleteById({id: row.entity.ID}, function onSuccess(response){
                if (response) {
                    message = 'User "' + row.entity.NAME + '" with id "' + deleteUser.ID+ '" was removed of your User\'s list';
                    toastr.success(message);
                    $scope.myData.splice(index, 1);
                    // $scope.audit_trail = {
                    //     fk_user : $rootScope.currentUser.ID,
                    //     fk_cabang : $rootScope.currentUser.fk_cabang,
                    //     fk_menu : 5,
                    //     fk_activitas_user : 30806,
                    //     keterangan : `Berhasil Delete User [Admin Menu], User ID : ${row.entity.ID}, Name : ${row.entity.NAME}, Cabang : ${row.entity.phone}, email : ${row.entity.email}, Status : ${row.entity.STATUS}`,
                    //     time_stamp : new Date()
                    // }
                    //  AuditTrail.create($scope.audit_trail);
                    return;
                }
                toastr.error('Failed to remove User data', 'Error');
            }, function onFailed(err){console.log('got error...'); toastr.warning('Failed to remove user data', 'Error');});
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this User?') : true;
    };
    if($state.current.url=='/user_view'){
        $scope.mode = 'view'
        $scope.addUser = false
        var removeRowTemplate = '<span style="display:block; text-align:center;margin-top:7px;"><button ng-click="viewRowIndex1(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></button></span>';
    }else{
        // var removeRowTemplate = `<span style="display:block; text-align:center;margin-top:7px;"><button ng-click="editRowIndex(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
        // <button ng-if="row.entity.STATUS=='Inactive' " ng-click="remove(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-times"></i></button>
        // </span>`;
         var removeRowTemplate = `<span style="display:block; text-align:center;margin-top:7px;"><button ng-click="editRowIndex(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
         </span>`;
    }
    $scope.gridOptions = { data: 'myData', enablePaging: true, showFooter: true, rowHeight: 36, headerRowHeight: 36,
    totalServerItems: 'totalServerItems', pagingOptions: $scope.pagingOptions, filterOptions: $scope.filterOptions, 
    keepLastSelected: true, multiSelect: false, sortInfo: $scope.sortOptions, useExternalSorting: true, enableColumnResize: true, enableColumnReordering :  true,
    columnDefs:[
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"5%"},
    {displayName:'ID/Username',field:'username',width:"15%"},
    {displayName:'Nama',field:'NAME',width:"55%"},
    // {displayName:'Cabang',field:'cabang.BRANCH_NAME',width:"30%"},
    {displayName:'Status',field:'STATUS',width:"30%"},
    // {displayName:'Approval Limit',field:'APPROVAL_LIMT',width:"30%"},
   
    ]};

    $scope.init = function init(){
        //  if($rootScope.currentUser.fk_cabang == 700){
           $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        // }
        $scope.Reset();
    }
    $scope.jabatans=Role.find();
    // $scope.cabangs= Utils.getCabangAktif();

    $scope.init();
}

function UserEditController ($scope, $rootScope, User, $stateParams,Role, $state, toastr, $filter, MstUserRole, Utils) {
    $scope.mode = 'entry'
    if($stateParams && $stateParams.mode) $scope.mode = $stateParams.mode
    // $scope.cabangs =  Utils.getCabangAktif()
    $scope.cabang = {};
    $scope.existingRole = {}; 
    $scope.userStatus = ['Active', 'Inactive'];
    $scope.user = {} 
    $scope.isEdit = true
    // Grid Role
    $scope.gridOptions = {
        data: 'myData',
        selectedItems: [],
        enableFiltering: false,
        rowHeight: 36,
        headerRowHeight: 36,
        columnDefs: [
            { displayName: ' Pilih Role / List',field: 'ROLE_NAME',width: "15%"},
        ]
    };

    $scope.init = function init() {
        $scope.myData = [];
        Role.find({
            filter:{
                where:{
                    STATUS : 'Active'    
                }
            }
        },function onSuccess(result){
          for(data of result){
            $scope.myData.push(data);
          }
        });
    }

    $scope.init();

    $scope.view = function view (){
        User.findById({id:$stateParams.ID, filter:{ }}, function onSuccess(data){
            console.log("User:", data)
            // if(data.STATUS == 'A'){
            //     data.STATUS = 'Active'; 
            // }else{
            //     data.STATUS = 'Inactive';
            // }
            data.mobile = Number(data.mobile) 
            $scope.user = data;
            if($scope.mode == 'entry'){
                // $scope.audit_trail = {
                //     fk_user : $rootScope.currentUser.ID,
                //     fk_cabang : $rootScope.currentUser.fk_cabang,
                //     fk_menu : 5,
                //     fk_activitas_user : 30804,
                //     keterangan : `Berhasil Akses Edit User [Admin Menu], User ID : ${data.ID}, Name : ${data.NAME}, Cabang : ${data.fk_cabang}`,
                //     time_stamp : new Date()
                // }
              }else{
                // $scope.audit_trail = {
                //     fk_user : $rootScope.currentUser.ID,
                //     fk_cabang : $rootScope.currentUser.fk_cabang,
                //     fk_menu : 5,
                //     fk_activitas_user : 30804,
                //     keterangan : `Berhasil Akses View User [Admin Menu], User ID : ${data.ID}, Name : ${data.NAME}, Cabang : ${data.fk_cabang}`,
                //     time_stamp : new Date()
                // }
            }
        //   AuditTrail.create($scope.audit_trail);  
            $scope.user.ID_Awal = $scope.user.id
            $scope.user.update_password = false;
            $scope.user.check_password = true;
            // if(data.FID_BRANCH){
            //     Cabang.findById({id:data.FID_BRANCH}, function onSuccess(cabang){
            //         console.log('cabang', cabang)
            //         $scope.user.cabang = cabang; 
            //     }); 
            // }
            
            MstUserRole.find({
                filter:{
                    where:{
                        IDF_USER : data.ID
                    }
                }
            }, function onSuccess (user_role){
                $scope.existingRole = user_role;
                for(active_role of user_role){
                    let i = 0;
                    for(list_role of $scope.myData){
                        if(active_role.IDF_ROLE == list_role.ID){
                            $scope.gridOptions.selectRow(i, true); 
                        }
                        i++;
                    }
                }
            }); 
        });  
    }

    $scope.view();

    // Save or update function
    $scope.disableSaveAll = false
    $scope.save = function save(user){
        toastr.info("Memproses...");
         $scope.disableSaveAll = true
        //console.log('$scope.currentUser?.ID',$scope.currentUser?.ID)
        // user.LAST_UPDATE_BY = $scope.currentUser?.ID;
        // user.modified = new Date(); 
        // user.ID = $scope.user.username; 
        // user.fk_cabang =  $scope.user.cabang.id;
        // user.FID_BRANCH =  $scope.user.cabang.id; 
        // if(user.STATUS == 'Active'){
        //     user.STATUS = 'A'; 
        // }else{
        //     user.STATUS = 'I';
        // }
        // console.log('user', user)
        // user.$save(function onSuccess(response){
        //             toastr.success('success update user')
        //         }, (err) =>{
        //             console.log('err', err)
        //             toastr.warning('Gagal update user')
        //         }); 
        //console.log('$scope.currentUser?.ID',$scope.currentUser?.ID)
        //  if( $scope.gridOptions.selectedItems.length > 1){
        //     toastr.warning('Hanya diperbolehkan pilih 1 role')
        //     return
        // }
       $scope.user.LAST_UPDATE_BY = $scope.currentUser?.ID;
       $scope.user.modified = new Date(); 
       //$scope.user.ID = $scope.user.username; 
    //    $scope.user.fk_cabang =  $scope.user.cabang.ID;
    //    $scope.user.FID_BRANCH =  $scope.user.cabang.ID; 
        // if($scope.user.STATUS == 'Active'){
        //    $scope.user.STATUS = 'A'; 
        // }else{
        //    $scope.user.STATUS = 'I';
        // }
        console.log('user',$scope.user)
         let editUser = new User($scope.user)
        console.log('editUser', editUser)
        // User.findById({id:$stateParams.ID}, function onSuccess(respUser){
        //     respUser.LAST_UPDATE_BY = $scope.currentUser?.ID
        //     respUser.modified = new Date()
        //     respUser.ID = $scope.user.username
        //     respUser.fk_cabang =  $scope.user.cabang.id
        //     respUser.LAST_UPDATE_BY = $scope.currentUser?.ID
        //     respUser.FID_BRANCH =  $scope.user.cabang.id
        //     respUser.STATUS = $scope.user.STATUS
        //     respUser.$save((res) =>{
        //             toastr.success('success update user')
        //         }, (err) =>{
        //             console.log('err', err)
        //             toastr.warning('Gagal update user')
        //         })
        // }, (err) =>{
        //     console.log('err', err)
        //     toastr.warning('Gagal find user')
        // })
        editUser.$save(function onSuccess(response){
            toastr.success('success update user');
                // $scope.audit_trail = {
                //     fk_user : $rootScope.currentUser.ID,
                //     fk_cabang : $rootScope.currentUser.fk_cabang,
                //     fk_menu : 5,
                //     fk_activitas_user : 30805,
                //     keterangan : `Berhasil Update User [Admin Menu], User ID : ${user.ID}, Name : ${user.NAME}, Cabang : ${user.fk_cabang}, email : ${user.email}, Status : ${user.STATUS}, Approval Limit : ${user.APPROVAL_LIMT}, jabatan : (${response.jabatan})`,
                //     time_stamp : new Date()
                // }
              
            // AuditTrail.create($scope.audit_trail);
             // deleting existing role
            for(exsist_role of $scope.existingRole){
                MstUserRole.deleteById({id : exsist_role.IDF_USER});             
            }
               //UPDATE Mst Role
            var jabatan_user = [];
            console.log('$scope.gridOptions.selectedItems', $scope.gridOptions.selectedItems)
            if($scope.gridOptions.selectedItems.length > 0){
                for(role of $scope.gridOptions.selectedItems){
                    Role.find({
                        filter:{
                            where:{ID : role.ID}
                        }
                }, function onSuccess(roles){
                    //console.log('roles',roles)
                    for(data of roles){
                        let user_role = {};     
                        user_role.IDF_ROLE = data.ID; 
                        user_role.IDF_USER = $scope.user.ID;
                        //jabatan_user.push(JSON.stringify(user_role.IDF_ROLE));
                        jabatan_user.push(user_role.IDF_ROLE); 
                        MstUserRole.create(user_role, function onSuccess(response){})
                    }
                    //console.log('jabatan_user', jabatan_user)
                    // let temp = jabatan_user.toString();
                    setTimeout(() =>{
                        let temp = jabatan_user.toString();
                        // User.findById({id:$scope.user.ID}, function onSuccess(result){
                             setTimeout(()=>{
                              response.jabatan = temp; 
                              response.$save();   
                            },1000)
                            //console.log("result:", result)
                        // /})    
                    },500)
                    
                })
                }
                toastr.success("save or updated success");
                setTimeout(()=>{
                        $state.go('component.user');
                },3500)
            }else{
                setTimeout(() =>{
                    // User.findById({id:$scope.user.ID}, function onSuccess(result){
                        response.jabatan = null; 
                        response.$save(); 
                        //console.log("result:", result)
                        toastr.success("save or updated success");
                        $state.go('component.user');
                    // })
                },2500)   
            }
            
        
        }, (err) =>{
            console.log('err', err)
                toastr.warning(`updated failed, ${err.data?.error?.message}`);
                $scope.disableSaveAll = false
        }); 
        console.log("save:", $scope.user)
       

     
    }

    // Cancel function
    $scope.cancel = function cancel(){
       $state.go('component.user');
    }
    $scope.cancelView = function cancelView(){
       $state.go('component.user_view1');
    }

    // State Go
    $scope.go = function go(url) {
        $location.path(url);
    }
     $scope.validasiPassword = () =>{
         if($scope.user.password.length < 8)
        {
             toastr.warning("Password minimal 8 karakter")
             $scope.invalidPassword = true
             return
        }else{
             $scope.invalidPassword = false
        }
        $scope.patt = new RegExp("^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])[a-zA-Z0-9@#$%^&+=]*$");
        $scope.resReg = $scope.patt.test($scope.user.password);
        console.log('$scope.resReg', $scope.resReg, $scope.user.password)
        if ($scope.resReg == true)
        {     $scope.invalidPassword = false
             return toastr.success("Password disetujui");
        }else{
             $scope.invalidPassword = true
           return toastr.warning("Password harus mengandung huruf besar, huruf kecil, Angka dan simbol");
        }
       
    }

}

function UserNewController ($scope, $rootScope, User, Role, $state, toastr, $timeout, MstUserRole, $stateParams, Utils) {
    $scope.addUser = true
    $scope.isEdit = false
    $scope.invalidPassword = false
    $scope.mode = 'entry'
    if($stateParams && $stateParams.mode) $scope.mode = $stateParams.mode
    //  $scope.audit_trail = {
    //     fk_user : $rootScope.currentUser.ID,
    //     fk_cabang : $rootScope.currentUser.fk_cabang,
    //     fk_menu : 5,
    //     fk_activitas_user : 30804,
    //     keterangan : `Berhasil Akses Add Entry Form User [Admin Menu]`,
    //     time_stamp : new Date()
    // }
    //   AuditTrail.create($scope.audit_trail); 
    $scope.user = {};
    $scope.user.update_password = true; 
    $scope.user.check_password = false; 
    // $scope.cabangs =  Utils.getCabangAktif()
    $scope.role = {};
    $scope.userStatus = ['Active', 'Inactive']; 

    // Grid Role
    $scope.gridOptions = {
        data: 'myData',
        selectedItems: [],
        enableFiltering: false,
        rowHeight: 36,
        headerRowHeight: 36,
        columnDefs: [
            { displayName: 'Pilih Role',field: 'ROLE_NAME',width: "15%"},
        ]
    };

    $scope.init = function init() {
        $scope.myData = [];
        Role.find({
            filter:{
                where :{status : 'Active'}
            }
        },function onSuccess(result){
          for(data of result){
            $scope.myData.push(data);
          }
        });
    }

    $scope.init();

    $scope.go = function go(url) {
        $location.path(url);
    }
    $scope.disableSaveAll = false
    $scope.save = function save(user){ 
         toastr.info("Memproses...");
        $scope.disableSaveAll = true

        console.log('user', user,$scope.gridOptions.selectedItems)
        // if( $scope.gridOptions.selectedItems.length > 1){
        //     toastr.warning('Hanya diperbolehkan pilih 1 role')
        //     return
        // }
        if(user.password != null){
            let jabatan_user = []; 
            $scope.user_role = {}; 
          //  user.fk_cabang = user.cabang.ID;
           // user.FID_BRANCH =user.cabang.ID; 
            user.ID = user.username; 
            // if(user.STATUS == 'Active'){
            //     user.STATUS = 'A'; 
            // }else{
            //     user.STATUS = 'I';
            // }   
            // if(user.cabang.ID == 700){
            //     user.LEVEL = 'PUSAT'; 
            // }else{
            //     user.LEVEL = 'CABANG';
            // }

            User.create(user, function onSuccess(response){
                // $scope.audit_trail = {
                //     fk_user : $rootScope.currentUser.ID,
                //     fk_cabang : $rootScope.currentUser.fk_cabang,
                //     fk_menu : 5,
                //     fk_activitas_user : 30804,
                //     keterangan : `Berhasil Add User [Admin Menu], User ID : ${response.ID}, Name : ${response.NAME}, Cabang : ${response.fk_cabang}, email : ${response.email}, Status : ${response.STATUS}, Approval Limit : ${response.APPROVAL_LIMT}, jabatan : (${response.jabatan})`,
                //     time_stamp : new Date()
                // }
                //  AuditTrail.create($scope.audit_trail);
                $timeout(function(){
                for(data of $scope.gridOptions.selectedItems){
                    Role.find({ 
                        filter:{
                            where:{ID : data.ID}
                        }
                    }, function onSuccess(role){
                        role.forEach(function(data){
                            $scope.user_role.IDF_USER  = user.ID; 
                            $scope.user_role.IDF_ROLE = data.ID; 
                            jabatan_user.push(data.ID);
                            MstUserRole.create($scope.user_role, function onSuccess(response){
                                //console.log("role:", response)
                            })
                        })
                        setTimeout(()=>{
                            User.findById({id:user.ID}, function onSuccess(result){
                                setTimeout(()=>{
                                 let temp = jabatan_user.toString();
                                 result.jabatan = temp; 
                                 result.$save();
                                },1000)
                                 
                            })
                        },500)
                    })
                  }
                  toastr.success("save or updated success");
                },300);
                setTimeout(()=>{
                 $state.go('component.user');
                },3500)
            },function onFailed(err){
                console.log('err', err)
                toastr.warning(`save or updated failed, ${err.data?.error?.message}`);
                  $scope.disableSaveAll = false
            });
        }else{
            toastr.warning("password is required");
            $scope.disableSaveAll = false 
        }     
    }

    $scope.cancel = function cancel(){
       $state.go('component.user');
    }
    $scope.validasiPassword = () =>{
         if($scope.user.password.length < 8)
        {
             toastr.warning("Password minimal 8 karakter")
             $scope.invalidPassword = true
             return
        }else{
             $scope.invalidPassword = false
        }
        $scope.patt = new RegExp("^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])[a-zA-Z0-9@#$%^&+=]*$");
        $scope.resReg = $scope.patt.test($scope.user.password);
        console.log('$scope.resReg', $scope.resReg, $scope.user.password)
        if ($scope.resReg == true)
        {    
            $scope.invalidPassword = false
             return toastr.success("Password disetujui");
        }else{
             $scope.invalidPassword = true
           return toastr.warning("Password harus mengandung huruf besar, huruf kecil, Angka dan simbol");
        }
       
    }
}
