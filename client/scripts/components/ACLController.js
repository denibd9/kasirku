angular.module('app').controller('ACLController', ACLController);
angular.module('app').controller('ACLEditController', ACLEditController);

function ACLController($q, $scope, $rootScope, $state, User, DTOptionsBuilder,
 DTColumnBuilder, $routeParams, $route, $filter, $location, toastr, $timeout) {
  $scope.go = function go(url) {
    $location.path(url);
  }

  $scope.editRowIndex = function editRowIndex(row, index) {
    $location.path('/component/acl/' + row.entity.model + '/edit');
  };

  var removeRowTemplate = '<span style="display:block; text-align:center;"><button ng-click="grid.appScope.editRowIndex(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button></span>';
  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    enableFiltering: false,
    rowHeight: 36,
    headerRowHeight: 36,
    columnDefs: [
      {
        displayName: 'Action',
        field: 'remove',
        cellTemplate: removeRowTemplate,
        width: "8%"
      },
      {
        displayName: 'Module',
        field: 'name',
      },
    ]
  };

  $scope.gridOptions.onRegisterApi = function onRegisterApi(gridApi) {
    $scope.gridApi = gridApi;

    $scope.gridApi.grid.registerRowsProcessor($scope.singleFilter, 200);
  }

  $scope.singleFilter = function(renderableRows) {
    var matcher = new RegExp($scope.searchKeyword);
    var filterCol = ['name'];
    renderableRows.forEach((row) => {
      var match = false;
      filterCol.forEach((field) => {
        if (row.entity[field].match(matcher))
          match = true;
      });
      if (!match) row.visible = false;
    });
    return renderableRows;
  };

  $scope.Bfilter = () => {
    $scope.gridApi.grid.refresh();
  }

  $scope.init = function init() {
    $scope.myData = $rootScope.appModule;
    // $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    // $scope.reset();
  }
  $scope.fullnames = 'MULKI SULAEMAN'
  $scope.usernames = 'MULKI'
  $scope.passwords = 'INDONESIA'
  $scope.emails = 'MULKI@GMAIL.COM'
  $scope.jabatans = ['OPERATOR', 'DIREKTUR'];
  $scope.cabangs = ['PAPUA', 'IRIAN'];
  $scope.init();
}


function ACLEditController($q, $scope, $rootScope, $state, User, DTOptionsBuilder, DTColumnBuilder, 
  $routeParams, $stateParams, $route, $filter, $location, toastr, $timeout,Role,Acl, $localStorage, Menu) {
  $scope.roles = Role.find({});
  $scope.accessType = $rootScope.aclAccessType;
  $scope.selectedAccess = [];
  $scope.acl = {};
  $scope.role = false;

  $scope.selectAllAccess = false;


  let getFormData = function(){
    var acls = [];
    var aclForm = $scope.acl;
    if ($scope.selectedAccessEqual()) {
      acls.push({
        model: $stateParams.model,
        principalType: "ROLE",
        principalId: aclForm.principalId.name,
        accessType: '*',
        permission: 'ALLOW'
      })
    } else {
      $scope.selectedAccess.forEach(function (el) {
        acls.push({
          model: $stateParams.model,
          principalType: "ROLE",
          principalId: aclForm.principalId.name,
          accessType: el,
          permission: 'ALLOW'
        })
      })
    }

    return acls;
  }
  

  $scope.save = function(){
    var acls = getFormData();
    let role = $scope.acl.principalId.name;
    let filter = {
      where: {model: $stateParams.model, principalId: role}
    }
    Acl.find({filter:filter},function(data){
      if (acls.length == 0  && data.length > 0) {
        data.forEach(function(el){ Acl.deleteById({id:el.id}) })
        toastr.success("Success to update acl");
      }else if(acls.length > 0){
        if (data.length > 0) 
          data.forEach(function(el){ ACL.deleteById({id:el.id}) });
        
        $timeout(function(){
          acls.forEach(function(el){
            Acl.create(el);
          })
          Menu.access({role: role}, function (data) {
            $localStorage['appMenus'] = data;
            $rootScope.appMenus = data;
          })
          toastr.success("Success to update acl");
        },300);
      }
    })

  }

  $scope.checkAllAccess = function(){
    $scope.selectAllAccess = $scope.selectAllAccess ? false: true;
    $scope.selectedAccess = [];
    if ($scope.selectAllAccess) {
      $scope.accessType.forEach(function(el){
        $scope.selectedAccess.push(el.value);
      })
    }
  }

  $scope.toggleSelection = function (value) {
    var idx = $scope.selectedAccess.indexOf(value);
    if (idx > -1)
      $scope.selectedAccess.splice(idx, 1);
    else
      $scope.selectedAccess.push(value);
  }

  $scope.roleSelect = function(){
    let role = $scope.acl.principalId.name;
    let filter = {
      where: {
        model:$stateParams.model,
        principalId: role
      }
    }
    Acl.find({filter: filter}, function (data) {
      $scope.selectAllAccess = false;
      $scope.selectedAccess = [];
      $scope.role = role;
      data.forEach(function(el){
        $scope.selectedAccess.push(el.accessType); 
      });

      if (data.length == 1 && data[0].accessType == '*') {
        $scope.accessType.forEach(function(el){
          $scope.selectedAccess.push(el.value);
        })
        $scope.selectAllAccess = true;
      }
      if ($scope.selectedAccessEqual()) {
        $scope.selectAllAccess = true;
      }

      console.log($scope.selectedAccess);
    });
  }

  $scope.selectedAccessEqual = function(){
    let map = $scope.accessType.map(function (el) {
      return el.value;
    }).sort();

    let results = $scope.selectedAccess.sort();

    return angular.equals(map, results);
  }
}