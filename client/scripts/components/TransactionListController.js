

angular
.module('app').controller('TransactionListController', TransactionListController);

function TransactionListController ($q,$scope,$filter, $rootScope, $state, $stateParams, Utils,
     $location, toastr, $timeout, Transaction) {

    //$scope.mode = String($stateParams.mode)

    $scope.mode = 'entry'
    
    //if($stateParams && $stateParams.mode) $scope.mode = $stateParams.mode
   
    // $scope.cabang = Cabang.find();
    $scope.filter = {};
    $scope.jenisBarangList = ['Konsumsi', 'Pembersih']
    $scope.kolomList = [
        {
        name : "Stok",
        code : "stok"
    },
    {
        name : "Jumlah Terjual",
        code : "jml_terjual"
    },
   ];
   $scope.urutanList = [
    {
    name : "Terbanyak",
    code : "DESC"
    },
    {
        name : "Terendah",
        code : "ASC"
    },
    ];
    
    $scope.addCustomer =  false
    $scope.editFin = false
    // $scope.corvorate = Utils.getMasterGeneric("jns_badan_usaha")
    // $scope.individu = Utils.getMasterGeneric("jns_badan_usaha_perorangan");
    // $scope.typecif = Utils.getMasterGeneric("tipe_cif")
    $scope.statusList = []
    // $scope.customerList = Customer.find({filter: {where : { tipe_customer : 1}}})
    $scope.disabled = true

    // $scope.cifChange = function(){
    //     if($scope.filter.tipecif.DESCRIPTION=='I-Individual'){
    //         $scope.typebu = $scope.individu
    //         $scope.disabled = false
    //     }else if($scope.filter.tipecif.DESCRIPTION=='C-Corporate'){
    //         $scope.typebu = $scope.corvorate
    //         $scope.disabled = false
    //     }
    // }

    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;

    $scope.pagingOptions = {pageSizes: [1000], pageSize: 1000, currentPage: 1};
        // sort
    $scope.sortOptions = { fields: ["id"], directions: ["ASC"]};
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        $scope.myData = data;
        $scope.unfilterData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function getPagedDataAsync() {
     
            const page = $scope.pagingOptions.currentPage;
            const pageSize = $scope.pagingOptions.pageSize;
            const offset = (page - 1) * pageSize;
            const filterParams = [];
            let order =''
            console.log('$scope.filter',$scope.filter)
            if ($scope.filter.nama_barang){
                filterParams.push({"nama_barang": {like : `%${$scope.filter.nama_barang}%`}});
            }
            if ($scope.filter.jenis_barang){
                filterParams.push({"jenis_barang": $scope.filter.jenis_barang});
            }
            if ($scope.filter.tgl_transaksi_awal){
                filterParams.push({"tgl_transaksi": { gte : $filter('date')(new Date($scope.filter.tgl_transaksi_awal),'yyyy-MM-dd')}});
            }
            if ($scope.filter.tgl_transaksi_akhir){
                filterParams.push({"tgl_transaksi": { lte : $filter('date')(new Date($scope.filter.tgl_transaksi_akhir),'yyyy-MM-dd')}});
            }
            if ($scope.filter.kolom && $scope.filter.sort){
                $scope.sortOptions = { fields: [$scope.filter.kolom.code], directions: [$scope.filter.sort.code]};
                order = `${$scope.filter?.kolom?.code} ${$scope.filter?.sort?.code}`
            }
            
            console.log('filterParams', filterParams)
            // if($scope.filter.assigment){
            //     if($scope.filter.assigment == 'YES'){
            //         filterParams.push({"fk_rm": $scope.currentUser.ID});
            //     }else if($scope.filter.assigment == 'NO'){
            //       filterParams.push({'fk_rm' : {neq: $scope.currentUser.ID}});
            //     }
            // }

            
            Transaction.find({filter:{where: { and: filterParams},include:[],offset: offset, limit: pageSize, order : order}}, function(data){
               // console.log("data Customer:", data);
               data.forEach((d,index)=>{
                d.no = index + 1
               })
                $scope.setPagingData(data,page,pageSize);
            })
            Transaction.count({where: { and: filterParams},offset: offset, limit: pageSize}, function onSuccess(rowscount){
                $scope.totalServerItems = rowscount.count;
            }, function onFailed(err){console.log('gagal find data:', err)});
            // unfilter = Customer.find({filter:{where:{fk_cabang:$scope.currentUser.fk_cabang},include:['cabang','Bentuk_badan_usaha','Tipe_cif','Tipe_customer'],offset: offset, limit: pageSize}})
     
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
            $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, 500);
    }, true);
    $scope.onTambah = ()=>{
        $scope.go("/transaction/new")
    }
    $scope.Bfilter = function Bfilter(argument) {
        console.log($scope.filter)
        $scope.init()
    }

    $scope.Brefresh = function Brefresh(argument) {
        $scope.filter = {}
        $scope.typebu = []
        $scope.disabled = true
        // if($rootScope.currentUser.fk_cabang!=700){

        // }
        // if($scope.mode == 'entry'){
        //     $scope.filter.cabang = Cabang.findById({id:$rootScope.currentUser.fk_cabang})
        // }
        if($scope.mode == 'entry'){
        // Cabang.findById({id:$rootScope.currentUser.fk_cabang},
        //     function onSuccess(res){
        //         $scope.filter.cabang = res;
        //         Customer.find({filter : {
        //             where : { fk_cabang : res.id }
        //         }},
        //         function onSuccess(rows){
        //             $scope.allData = rows;},
        //         function onFailed(err){console.log('gagal find data:', err)
        //         });
        //     })
    }
        setTimeout(function(){

            // console.log($scope.filter)
            $scope.init();
        }, 1000)
    }

    $scope.go = function go(url){
        $location.path(url);
    }
    $scope.editRowIndex = function editRowIndex(row, index){
        //var id = $scope.myData[index].id;
        //console.log('edit ID:', id, index);
       console.log('edit ID:', row.entity);
        $location.path('/transaction/'+row.entity.id+'/edit');
    };
 
    $scope.remove = function remove( row, index, confirmation ){
        // var id = $scope.myData[index].id;
        var id = row.entity.id;
        var deletedregion = row.entity;

        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {

            var message;
            Transaction.deleteById({id: id}, function onSuccess(response){
                if (response) {

                    message = "Transaksi : '" + row.entity.nama_barang + "' with id '" + deletedregion.id+ "' was removed of your Transaction\'s list";
                    toastr.success(message);
                    $scope.myData.splice(index, 1);
                    return;
                }

            }, function onFailed(err){
                console.log('got error...');
                toastr.warning('Failed to remove Jobsheet data', 'Error');

            });
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Transaction?') : true;
    };
    if($scope.mode != 'view'){
        var actionMenu = `<span style="display:block; text-align:center;margin-top:7px;">
        <div >
            <button ng-click="editRowIndex(row, $parent.$index)" class="btn btn-xs btn-default">
                <i class="fa fa-pencil"></i>
            </button>&nbsp
            <button ng-click="remove(row, $parent.$index)" class="btn btn-xs btn-default">
                <i class="fa fa-times"></i>
            </button>&nbsp
        </div>`
    }else{
        var actionMenu = `<span style="display:block; text-align:center;margin-top:7px;">
        <div >
            <button ng-click="viewRowIndex(row, $parent.$index)" class="btn btn-xs btn-default">
                <i class="fa fa-eye"></i>
            </button>
        </div>`
    }
        

    // Grid List Customer
    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        enableColumnResize: true,
        enableColumnReordering :  true ,
        enableCellSelection :  true ,
        rowHeight: 36,
        headerRowHeight: 36,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        keepLastSelected: true,
        multiSelect: false,
        sortInfo: $scope.sortOptions,
        useExternalSorting: true,
        columnDefs:[
            {displayName:"Action", cellTemplate: actionMenu ,width: "80px"},
            {displayName:'No',field:'no', width:"50px"},
            {displayName:'Nama Barang',field:'nama_barang', width:"200px"},
            {displayName:'Stok',field:'stok', width:"150px"},
            {displayName:'Jumlah Terjual',field:'jml_terjual', width:"150px"},
            {displayName:'Tanggal Transaksi',field:'tgl_transaksi', type: 'date', cellFilter: 'date:\'dd-MM-yyyy\'', width:"150px"},
           {displayName:'Jenis Barang',field:'jenis_barang', width:"150px"},
            
           
        ]
    };

    var rating = $scope.gridOptions.columnDefs.map(function (e) {return e.cellTemplate;}).indexOf('<div ng-click="rating(row, $parent.$index)" class="ui-grid-cell-contents">Rating</div>');

   

    $scope.init = function init(){
       
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.Brefresh();
    // $scope.onExportData = function(searchText){
    //     toastr.info('Mengexport...')
    //     var totrows = $scope.totalServerItems;
    //     $scope.pageSize = 2000;
    //     $scope.totpages = Math.ceil($scope.totalServerItems / $scope.pageSize);
    //     var custacqui = [];
    //     var promises = [];
    //     var ft = $scope.searchTextEXS;
    //     let filterParams = [];

    //         if ($scope.filter.code){
    //             filterParams.push({"code": {like : `%${$scope.filter.code}%`}});
    //         }
    //         if ($scope.filter.name){
    //             filterParams.push({"name": {like : `%${$scope.filter.name}%`}});
    //         }
    //         if ($scope.filter.jobsheetDt){
    //             filterParams.push({"jobsheetDt": $filter('date')(new Date($scope.filter.jobsheetDt),'yyyy-MM-dd')});
    //         }

    //         // if ($scope.filter.code){
    //         //     filterParams.push({"code": {like : `%${$scope.filter.kodeResi}%`}});
    //         // }
    //         if ($scope.filter.Customer){
    //             filterParams.push({"fkCustomer": $scope.filter.Customer.id});
    //         }
    //         if ($scope.filter.namaPengirim){
    //             filterParams.push({"namaPengirim": {like : `%${$scope.filter.namaPengirim}%`}});
    //         }
    //         if ($scope.filter.Status){
    //             filterParams.push({"fkStatus": $scope.filter.Status.id});
    //         }
    //         if ($scope.filter.remark){
    //             filterParams.push({"remark": {like : `%${$scope.filter.remark}%`}});
    //         }
    //         console.log('filterParams', filterParams)
           
    //     function callUpdate(x, promises,it){
    //         //alert('ok')
    //         let d = $q.defer();
    //          let filterParams = [];
    //           Jobsheet.find({filter:{where: { and: filterParams},include:['Customer','Status','CurrentPoint','NextPoint'],
    //           order: 'id', 
    //           offset : x.offset, 
    //           limit : $scope.pageSize}}, function(list){
    //             // console.log('list', list)
    //                 list.forEach((jobsheet,i)=>{
    //                      custacqui.push({
    //                             'NO Resi' : jobsheet.code,
    //                             'Order' : jobsheet.name ,
    //                             'Tgl Order' : jobsheet.jobsheetDt ,
    //                             'Customer' : jobsheet.name ,
    //                             'Pengirim' : jobsheet.alamatPengirim ,
    //                             'Current Point' : jobsheet?.CurrentPoint?.name ,
    //                             'Next Point' : jobsheet?.NextPoint?.name ,
    //                             'Status' : jobsheet?.Status?.DESCRIPTION ,
    //                             'Remark' : jobsheet.remark ,
    //                             });
    //                         if(i == list.length - 1){//0 == 1-1
    //                          d.resolve();
    //                         }                       
    //                 })
    //             }, function onFailed(err){console.log('gagal find data:', err)});
            
    //         promises.push(d.promise);
    //     }

    //     for(let i = 0; i < $scope.totpages; i++){
    //         setTimeout(()=>{
    //             $scope.offset = i * $scope.pageSize;
    //             console.log(`$scope.offset (${$scope.offset}), i ${i}`)
    //             callUpdate({offset: $scope.offset}, promises, i);
    //         },100)
            
    //     }
    //      for(let i = 0; i < $scope.totpages; i++){
    //         setTimeout(()=>{
    //             toastr.info('Mengexport...', {timeOut: 100})
    //             toastr.remove()
    //         },1000 * i )
    //     }
        
    //     setTimeout(()=>{


    //     $q.all(promises).then(function(_data){
    //         //console.log('jumlah rows:', custacqui, _data);
    //         custacqui = custacqui.sort((a,b)=>{
    //             return b.ID - a.ID
    //         })
    //         alasql('SELECT * INTO XLSX("cargo_order.xlsx", {headers: true}) FROM ? ',[custacqui]);
    //     });
    //      },5000)
    // }
}
