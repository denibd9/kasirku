angular
.module('app')
.controller('AuthController', AuthController)
;

angular
.module('app')
.controller('LogoutController', LogoutController)
;

function AuthController($scope, AuthService, MstUserRole,deviceDetector,$state,Cabang, User, $rootScope, LoopBackAuth, $location, toastr,Role, $localStorage, Menu) {
  $scope.user = {};
  $scope.activeStatus = true
  $scope.invalidPassword = false
//   $.getJSON('https://api.db-ip.com/v2/free/self', function(data) {
//       console.log('info login', JSON.stringify(data, null, 2));
//       $scope.loginInfo = JSON.stringify(data, null, 2)
//   });
//   $.get('https://www.cloudflare.com/cdn-cgi/trace', function(data) {
//   // Convert key-value pairs to JSON
//   // https://stackoverflow.com/a/39284735/452587
//   data = data.trim().split('\n').reduce(function(obj, pair) {
//     pair = pair.split('=');
//     return obj[pair[0]] = pair[1], obj;
//   }, {});
//   console.log('1',data);
//    $scope.loginInfo1 = data
// });
$scope.loginInfo = {}
  $.getJSON('https://json.geoiplookup.io/?callback=?', function(data) {
  console.log('2',data);
   $scope.loginInfo = data
});
  $scope.onSuccess = () =>{
    alert('ok')
  }
  $scope.onError = () =>{
    alert('error')
  }
  var vm = this;
  vm.data = deviceDetector;
  console.info("Detector",vm.data.browser);
  vm.allData = JSON.stringify(vm.data, null, 2);
  $scope.logout = function(){
    // $localStorage["appMenus"] = null;
    // $localStorage[LoopBackAuth.accessTokenId] = null;
    User.logout().$promise
    .then(function() {
      setTimeout(function() {
        $window.location.reload();
      }, 10);
    });
    $rootScope.currentUser = null;
    $localStorage.$reset();
  };
  
  $scope.loadLogin = function loadLogin() {
    $scope.login = function() {
        //  if($scope.user.password.length < 8)
        // {
        //      toastr.warning("Password minimal 8 karakter")
        //      $scope.invalidPassword = true
        //      return
        // }else{
        //      $scope.invalidPassword = false
        // // }
        // $scope.patt = new RegExp("^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])[a-zA-Z0-9@#$%^&+=]*$");
        // $scope.resReg = $scope.patt.test($scope.user.password);
        // console.log('$scope.resReg', $scope.resReg, $scope.user.password)
       
      console.log('USER LOGIN: ', $scope.user, $scope.rememberMe)
      User.login({username: $scope.user.username, password: $scope.user.password, rememberMe: $scope.rememberMe}).$promise
      .then(function(response) {
        response.user.Role = []
        
        setTimeout(function() {
          console.log('res login', response)
        $rootScope.currentMenu = {name : "btns.dashboard"};
        console.log("Data Login First Time",response)
        currUser = response.user;
        var next = $location.nextAfterLogin || '/';
        $rootScope.currentUser = angular.copy(response.user);
        if(response.user.STATUS!='A'){
          $scope.activeStatus = false
        }
        $localStorage[''+response.id] = response.user;

            setTimeout(()=>{
              $state.go('btns.dashboard');
            },1000)
        
        },1000);
      }, function(err){
        console.log('err', err)
         toastr.warning('username atau password salah atau username tidak terdaftar (Incorrect user credentials)')
        console.log('face error during login');
        
      })
      // . catch(error){
      //   toastr.warning('Password atau username anda salah (Incorrect user credentials)')
      // }
         
    };
  }
  console.info("Browser Detector",vm.allData);
  if (vm.data.browser == "chrome") {
    $scope.loadLogin();
  }else if(vm.data.browser == "ie") {
    if (vm.data.browser_version >= 9) {
      $scope.loadLogin();
    } else {
     toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.') 
     $scope.login = function() {
      toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.')
    };
  }
}else if(vm.data.browser == "safari") {
  $scope.loadLogin();
}else if (vm.data.browser == "firefox") {
  if (vm.data.browser_version >= 51) {
    $scope.loadLogin();
  } else {
   toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.') 
   $scope.login = function() {
    toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.')
  };
}
} else if (vm.data.browser == "ms-edge") {
  $scope.loadLogin();
} else{
  $scope.login = function() {
    toastr.error('Oops, your web browser is no longer supported. Please use Google Chrome or Safari instead.')
  };
  toastr.error('Oops, your web browser is no longer supported. Please use Google Chrome or Safari instead.')
}

 
  
}

function LogoutController ($rootScope, $state, $scope, User, LoopBackAuth, $localStorage, $window) {
  $scope.logout = function(){
    // $localStorage["appMenus"] = null;
    // $localStorage[LoopBackAuth.accessTokenId] = null;
    if( $rootScope.currentUser) {
      $scope.currentUser = $rootScope.currentUser
    }else{
      $scope.currentUser = null
    }
    User.logout().$promise
    .then(function() {
       if( $scope.currentUser) {
          
      }else{
        
      }
      setTimeout(function() {
            $window.location.reload();
            $state.go('auth.signin');
          }, 1000);
    });
    $rootScope.currentUser = null;
    $localStorage.$reset();
  };
  $scope.logout();
}
