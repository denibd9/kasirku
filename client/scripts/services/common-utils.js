

angular
	.module('app')
	.factory('Utils', Utils);

function Utils(MasterGeneric, toastr, Dokumen, Cabang) {
	// 
	const GENERICS = []
	function getMasterGeneric(g_type, cb) {
		var G = GENERICS.filter(obj=>obj.GENERIC_TYPE==g_type);
		if (G && G.ttl > moment()) {return G.data}
			MasterGeneric.find({filter:{where:{GENERIC_TYPE:g_type, STATUS:'A'}}}, 
				function onsuccess(rest){
					if(rest.date2 && (rest.date2 > $scope.dateNow)){
	                    rest.STATUS = 'Inactive'
	                    rest.$save();
	                }else if(rest.date2 && (rest.date2 < $scope.dateNow)){
	                    rest.STATUS = 'Active'
	                    rest.$save();
	                } 
				}, 
				function onerr(err, rest){if(cb) cb(err, data)}
			)

			
		return MasterGeneric.find({filter:{where:{GENERIC_TYPE:g_type, STATUS:'Active'}}}, 
			function onsuccess(resp){
				G = {GENERIC_TYPE: g_type, data: resp, ttl: moment().add(2, 'day')}
				GENERICS.push(G);

				if(cb) cb(null, resp)
			}, 
			function onerr(err, data){if(cb) cb(err, data)}
			);
	};

	function toFlo(field) {
		var inFl = (field ? parseFloat(field) : parseFloat(0.0));
		if (inFl) {
			return inFl;
		} else {
			return 0
		}

	};

	function getCabangAktif(){
		return Cabang.find({filter : {where : { STATUS : 'A'}}})
	}

	function validateNumber(val, max, min, positveOnly) {
		!positveOnly ? positveOnly = true : positveOnly = positveOnly;
		if (val < 0 && positveOnly) return val * -1
		if (max) if (val > max) return max
		if (min) if (val < min) return min
		return val
	}

	function disableNumberSymbol(charCode) {
		if (charCode == 101 || charCode == 69 || charCode == 45 
			|| charCode == 43 || charCode == 189 || charCode == 187) {
			return false;
		}
		return true;
	}

	function validateKeyNumber(value) {
		if (value == null)
			return ""
		if (!value == "") {
			if (!((value.charCodeAt(value.length - 1) >= 48 && value.charCodeAt(value.length - 1) <= 57))) {
				return value.slice(0, value.length - 1)
			}
		}
		return value;
	}

	function getLatestDate(data) {
		if (data.length > 0) {
			var value = data.reduce((a, b) => {
				return new Date(a.MeasureDate) > new Date(b.MeasureDate) ? a : b;
			})
			return value
		}
	}

	function toRound(value) {
		if (value) {
			var Round = Math.round((value) * 10 ** 6) / 10 ** 6
		} else {
			var Round = 0
		}
		return Round;
	};

	function toRound2(value) {
		if (value) {
			var Round = Math.round((value) * 100) / 100
		} else {
			var Round = 0
		}
		return Round;
	};

	function validateKeyCharOnly(value) {
		if (value == null)
			return ""
		if (!value == "") {
			if (!(
				(value.charCodeAt(value.length - 1) >= 95 && value.charCodeAt(value.length - 1) <= 122) ||
				(value.charCodeAt(value.length - 1) >= 65 && value.charCodeAt(value.length - 1) <= 90))
			) {
				return value.slice(0, value.length - 1)
			}
		}
		return value;
	}

	function validateString(val, atr) {
		var TCode = val;
		if (/^[a-zA-Z() ]+$/.test(TCode)) {
			return true;
		}
		toastr.warning(atr + ' ' + 'tidak boleh mengandung angka, special character.')
		return false;
	}

	function validateAlphaNumeric(value) {
		if ( /^[a-zA-Z0-9\-\s]+$/.test(value)) {
			//ngModel.$setValidity('alphanumeric', true);
			return value;
		} else {
			//ngModel.$setValidity('alphanumeric', false);
			return undefined;
		}
	};

	function checkAvailableFile(file){
		let available_ext = [
			"image/jpeg", //jpeg
			"image/jpg", //jpg
			"application/pdf", //pdf
			"application/msword", //doc
			"application/vnd.openxmlformats-officedocument.wordprocessingml.document", //docx
		]
		let max_size = 2000000;
		if(file.size > max_size){
			return "Max file size is 2MB";
		}
		if(available_ext.find(el => el == file.type)){
			return null			
		}
		return "File type not allowed"
	}

	function localPaging(rows, page, pageSize) {
		var pageNum = (rows.length / pageSize) + (rows.length / pageSize > 0 ? 1 : 0);
		if (pageNum > page) {
			var offset = (page - 1) * pageSize;
			return rows.slice(offset, offset + pageSize);
		} else {
			var offset = (pageNum - 1) * pageSize;
			var lastPageSize = rows.length / pageSize;
			return rows.slice(offset, offset + lastPageSize);
		}
	}

	function sortArrayByDate(arr, ASC, field) {
		var newArr = []
		if (!arr || arr.length == 0) { return null; }
		if (ASC) {
			newArr = arr.sort(function (a, b) {
				if (a[field] > b[field]) return -1;
				if (a[field] < b[field]) return 1;
				return 0;
			})
		} else {
			newArr = arr.sort(function (a, b) {
				if (a[field] > b[field]) return 1;
				if (a[field] < b[field]) return -1;
				return 0;
			})
		}

		return newArr;
	}

	function sortArrayByDateGetMax(arr, ASC, field) {
		var newArr = []
		if (!arr || arr.length == 0) { return null; }
		if (ASC) {
			newArr = arr.sort(function (a, b) {
				if (a[field] > b[field]) return -1;
				if (a[field] < b[field]) return 1;
				return 0;
			})[0]
		} else {
			newArr = arr.sort(function (a, b) {
				if (a[field] > b[field]) return 1;
				if (a[field] < b[field]) return -1;
				return 0;
			})[0]
		}

		return newArr;
	}

	function getIndexFormList(currentPage,pageSize,view_index){
        return ((currentPage - 1) * pageSize)  + view_index;
	}

	function checkExistingFile(file_name, cb){
		return Dokumen.find({ filter : {where : {filename : file_name}}},
			function onsuccess(resp){
				if(cb) cb(null, resp)
			}, 
			function onerr(err, data){if(cb) cb(err, data)}
		);
	}

	///baru
	let $scope = {};
    let Model = {};
    const LOCATION_DATA = {
        Provinsi : { data : [], ttl : 0},
        Kota : { data : [], ttl : 0},
        Kelurahan : { data : [], ttl : 0},
        Kecamatan : { data : [], ttl : 0}
    }

    // function getAppData(){
    //     let appData = {};
    //     appData.appVersion = 1.0;
    //     appData.appName = 'Vending Machine';

    //     return appData;
    // }

	function getProvinsi(cb){
        var provinsi = LOCATION_DATA.Provinsi;
        if(provinsi && LOCATION_DATA.Provinsi.ttl > moment()){return provinsi.data }

        return Provinsi.find({},
        function onsuccess(resp){
            LOCATION_DATA.Provinsi.data = resp;
            LOCATION_DATA.Provinsi.ttl =  moment().add(2, 'day');
            if(cb) cb(null, resp)
        },
            function onerr(err, data){if(cb) cb(err, data)}
        );

    }

    function getProvinsiByRegion(region,cb){
        console.log("masuk get province");
        var provinsi = LOCATION_DATA.Provinsi.data.filter(el => el.REGION_NAME == region);
        if(provinsi.length > 0 && LOCATION_DATA.Provinsi.ttl > moment()){return provinsi.data }

        return Provinsi.find({filter : {where : {REGION_NAME: region}}},
        function onsuccess(resp){
            LOCATION_DATA.Provinsi.data = Object.assign(LOCATION_DATA.Provinsi.data,resp);
            LOCATION_DATA.Provinsi.ttl =  moment().add(2, 'day');
            if(cb) cb(null, resp)
        },
            function onerr(err, data){if(cb) cb(err, data)}
        );

    }

    function getKota(ID,cb){
        console.log("masuk get kota");
        var kota = LOCATION_DATA.Kota.data.filter(el => el.FID_PROVINCE == ID);
        if(kota.length > 0 && LOCATION_DATA.Kota.ttl > moment()){return kota }

        return Kota.find({filter : {where : {FID_PROVINCE : ID}}},
        function onsuccess(resp){
            LOCATION_DATA.Kota.data = Object.assign(LOCATION_DATA.Kota.data,resp);
            LOCATION_DATA.Kota.ttl =  moment().add(2, 'day');

            if(cb) cb(null, resp)
        },
            function onerr(err, data){if(cb) cb(err, data)}
        );
    }

    function getKelurahan(ID,cb){
        var kelurahan = LOCATION_DATA.Kelurahan.data.filter(el => el.FID_CITY_REGENCY == ID);

        console.log("ttl, length",LOCATION_DATA.Kelurahan.ttl > moment() , kelurahan.length)

        if(kelurahan.length > 0 && LOCATION_DATA.Kelurahan.ttl > moment()){return kelurahan }

        console.log("get new data")
        return Kelurahan.find({filter : {where : {FID_CITY_REGENCY : ID}}},
        function onsuccess(resp){
            LOCATION_DATA.Kelurahan.data = Object.assign(LOCATION_DATA.Kelurahan.data,resp);
            LOCATION_DATA.Kelurahan.ttl =  moment().add(2, 'day');

            if(cb) cb(null, resp)
        },
            function onerr(err, data){if(cb) cb(err, data)}
        );

    }

    function getKecamatan(ID,cb){
        var kecamatan = LOCATION_DATA.Kecamatan.data.filter(el => el.FID_DISTRICT == ID);
        if(kecamatan.length > 0 && LOCATION_DATA.Kecamatan.ttl > moment()){return kecamatan }

        return Kecamatan.find({filter : {where : {FID_DISTRICT : ID}}},
        function onsuccess(resp){
            LOCATION_DATA.Kecamatan.data = Object.assign(LOCATION_DATA.Kecamatan.data,resp);
            LOCATION_DATA.Kecamatan.ttl =  moment().add(2, 'day');

            if(cb) cb(null, resp)
        },
            function onerr(err, data){if(cb) cb(err, data)}
        );
    }
	function validateNumber(val, max, min, positveOnly) {
		!positveOnly ? positveOnly = true : positveOnly = positveOnly;
		if (val < 0 && positveOnly) return val * -1
		if (max) if (val > max) return max
		if (min) if (val < min) return min
		return val
	}

	function disableNumberSymbol(charCode) {
		if (charCode == 101 || charCode == 69 || charCode == 45
			|| charCode == 43 || charCode == 189 || charCode == 187) {
			return false;
		}
		return true;
	}

	function validateKeyNumber(value) {
		if (value == null)
			return ""
		if (!value == "") {
			if (!((value.charCodeAt(value.length - 1) >= 48 && value.charCodeAt(value.length - 1) <= 57))) {
				return value.slice(0, value.length - 1)
			}
		}
		return value;
	}

	function getLatestDate(data) {
		if (data.length > 0) {
			var value = data.reduce((a, b) => {
				return new Date(a.MeasureDate) > new Date(b.MeasureDate) ? a : b;
			})
			return value
		}
	}


	function validateKeyCharOnly(value) {
		if (value == null)
			return ""
		if (!value == "") {
			if (!(
				(value.charCodeAt(value.length - 1) >= 95 && value.charCodeAt(value.length - 1) <= 122) ||
				(value.charCodeAt(value.length - 1) >= 65 && value.charCodeAt(value.length - 1) <= 90))
			) {
				return value.slice(0, value.length - 1)
			}
		}
		return value;
	}

    let actionTemplate = '<span style="display:block; text-align:center; margin-top : 5px;">'+
    '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
    '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
    '<i class="fa fa-times"></i></button></span>';

    function scopeInitialization(scopeParam){
        $scope = scopeParam;
    }

    function modelInitialization(modelParam){
        Model = modelParam;
    }

    function gridInitialization(columnDefs, _mydata){
        $scope.gridOptions = [];
      $scope.filter = { externalFilter: false };
      if (_mydata) {
        $scope[_mydata] = []
      } else {
        $scope.myData = [];
      }
        $scope.exisitingData = [];
        $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage : 1};
        $scope.sortOptions = [];
        $scope.gridOptions = {
            data: _mydata||'myData',
            enablePaging: true, rowHeight: 36, headerRowHeight: 50,
            paginationPageSizes : $scope.pagingOptions.pageSizes,
            paginationPageSize: $scope.pagingOptions.pageSize,
            enableGridMenu: true,
            useExternalPagination: true,
            useExternalSorting: true,
            enableColumnResizing : true,
            columnDefs:columnDefs,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                    var sortQuery = [];
                    sortColumns.forEach( el => {
                        let sortElement = "";
                        sortElement = el.field + " " + el.sort.direction.toUpperCase();
                        sortQuery.push(sortElement);
                    })
                    $scope.sortOptions = Object.assign(sortQuery,sortQuery);
                    getPagedDataAsync();
                });
                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    $scope.pagingOptions.pageSize = pageSize;
                    $scope.pagingOptions.currentPage = newPage;
                    getPagedDataAsync();
                });
            }
        };
    };

    function onSuccessCountModelData(){
        return function(response){
            $scope.gridOptions.totalItems = response.count;
        };
    }

    let beforeSetPagingDataCB = function(data){};
    function setBeforeSetPagingDataCB(cb){ beforeSetPagingDataCB = cb; };

    function setPagingData(data){
        beforeSetPagingDataCB(data);
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    function onSuccessFindModelData(){
        return function(response){
            setPagingData(response);
        }
    }

    function onFailedCallingLBService(){
        return function(err){
            console.log(err);
            toastr.info("Failed to connect to database", "Information");
        }
    }

    function getPagedDataAsync(){
        let countQuery = {};
        let findQuery = {
            filter : {
                order : $scope.sortOptions,
                limit : $scope.pagingOptions.pageSize,
                skip : ($scope.pagingOptions.currentPage - 1) * $scope.pagingOptions.pageSize
            }
        };
        if($scope.filter.externalFilter){
            countQuery = $scope.filterQuery;
            findQuery.filter = Object.assign(findQuery.filter, $scope.filterQuery);
        };
        Model.count(countQuery, onSuccessCountModelData(), onFailedCallingLBService());
        Model.find(findQuery, onSuccessFindModelData(), onFailedCallingLBService());
    };

    function populateParams(params){
        let paramCode, onSuccess;
        for(let param of params){
            if(typeof param === 'object'){
                paramCode = param.paramCode;
                onSuccess = function onSuccess(result){
                    for(let data of result.params){
                        $scope[param.filterName].push(data[param.paramPropertyToBeAssign]);
                    }
                }
            }else{
                paramCode = param;
                onSuccess = function(res){
                    $scope[param] = res.params;
                };
            }
            Param.findOne(
                {filter : {where : { code : paramCode}}},
                onSuccess
            );
        };
    };

    let onSuccessFindingModelWhenDeletingRow = function(response){
        toastr.success("Delete data success", "Success")
        getPagedDataAsync();
    };

    function removeRow(row, index, confirmation){
        let id = row.entity.id;
        let confirmDelete = function confirmDelete(confirmation){
            return confirmation ? confirm('This action is irreversible. Do you want to delete this data?') : true;
        };
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            Model.deleteById(
                {id:id},
                onSuccessFindingModelWhenDeletingRow,
                function onFailed(err){
                    toastr.warning(err.data.error.message, "Warning")
                }
            )
        }
    }

    function checkIfFilterFilledIn(filterNames){
        $scope.filter.externalFilter = false;
        for(let filterName of filterNames){
            if($scope.filter[filterName]){
                $scope.filter.externalFilter = true;
                break
            }
        }
    }

    function assignFilterToQueryParams(filters){
        $scope.filterQuery = { where : { and : []}};
        for(let filterName in filters){
            if($scope.filter[filterName]){
                $scope.filterQuery.where.and.push({[filterName] : filters[filterName]});
            }
        }
    }

	return {
		checkExistingFile : checkExistingFile,
		checkAvailableFile : checkAvailableFile,
		getIndexFormList : getIndexFormList,
		disableNumberSymbol: disableNumberSymbol,
		validateKeyCharOnly: validateKeyCharOnly,
		validateKeyNumber: validateKeyNumber,
		getMasterGeneric: getMasterGeneric,
		toFlo: toFlo,
		validateNumber: validateNumber,
		validateString: validateString,
		localPaging: localPaging,
		sortArrayByDate: sortArrayByDate,
		toRound: toRound,
		toRound2: toRound2,
		getLatestDate: getLatestDate,
		sortArrayByDateGetMax: sortArrayByDateGetMax,
		validateAlphaNumeric : validateAlphaNumeric,
		getCabangAktif : getCabangAktif,

		scopeInitialization: scopeInitialization,
        modelInitialization: modelInitialization,
        gridInitialization: gridInitialization,
        getPagedDataAsync: getPagedDataAsync,
        populateParams: populateParams,
        removeRow: removeRow,
        actionTemplate: actionTemplate,
        checkIfFilterFilledIn: checkIfFilterFilledIn,
        assignFilterToQueryParams: assignFilterToQueryParams,
        getProvinsi : getProvinsi,
        getProvinsiByRegion : getProvinsiByRegion,
        getKota : getKota,
        getKelurahan : getKelurahan,
        getKecamatan : getKecamatan,
        validateNumber : validateNumber,
        disableNumberSymbol : disableNumberSymbol,
        validateKeyNumber : validateKeyNumber,
        getLatestDate : getLatestDate,
        validateKeyCharOnly : validateKeyCharOnly,
        setBeforeSetPagingDataCB:setBeforeSetPagingDataCB,
        onSuccessFindingModelWhenDeletingRow: onSuccessFindingModelWhenDeletingRow,


	};
};