'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
 angular.module('d3', [])
 .factory('d3Service', [function(){
    var d3;
    // insert d3 code here
    return d3;
}]);
var BTNS = angular
  .module('app', [
    
    'd3',
    'lbServices',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngMaterial',
    'ngStorage',
    'ngStore',
    'ngGrid',
    'ngRoute',
    'ui.grid',
    'ui.router', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav',
    'ui.utils',
    'ui.bootstrap',
    'ui.load',
    'ui.jp',
    'pascalprecht.translate',
    'oc.lazyLoad',
    'angular-loading-bar',
    'ui.select', 
    'toastr', 
    'datatables',
    'dynamicNumber',
    'ngMap',
    'angular-js-xlsx',
    'lr.upload',
    'angularFileUpload',
    '720kb.datepicker',
    'ng.deviceDetector',
    
    //blur admin

    // 'smart-table',
    // 'xeditable',
    // 'ui.slimscroll',
    // 'ngJsTree',
    // 'angular-progress-button-styles',

    // 'BlurAdmin.theme',
    // 'BlurAdmin.pages'
// 
    //end 
    // ,

    // 'moment-picker'
  ]);
