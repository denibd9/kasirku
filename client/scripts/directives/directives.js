'use strict';

angular.module('app')
  .directive('currencyInput', function ($filter, $browser) {
    return {
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModelCtrl) {
        var listener = function () {
          var value = $element.val().replace(/,/g, '')
          $element.val($filter('number')(value, false))
        }

        // This runs when we update the text field
        ngModelCtrl.$parsers.push(function (viewValue) {
          return viewValue.replace(/,/g, '');
        })

        // This runs when the model gets updated on the scope directly and keeps our view in sync
        ngModelCtrl.$render = function () {
          $element.val($filter('number')(ngModelCtrl.$viewValue, false))
        }

        $element.bind('change', listener)
        $element.bind('keydown', function (event) {
          var key = event.keyCode
          // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
          // This lets us support copy and paste too
          if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40))
            return
          $browser.defer(listener) // Have to do this or changes don't get picked up properly
        })

        $element.bind('paste cut', function () {
          $browser.defer(listener)
        })
      }
    }
  });

angular.module('app')
  .directive('scroll', [function () {
    return {
      link: function (scope, element, attrs) {
        // ng-repeat delays the actual width of the element.
        // this listens for the change and updates the scroll bar
        function widthListener() {
          if (anchor.width() != lastWidth)
            updateScroll();
        }

        function updateScroll() {
          // for whatever reason this gradually takes away 1 pixel when it sets the width.
          $div2.width(anchor.width() + 1);
          // make the scroll bars the same width
          $div1.width($div2.width());
          // sync the real scrollbar with the virtual one.
          $wrapper1.scroll(function () {
            $wrapper2.scrollLeft($wrapper1.scrollLeft());
          });

          // sync the virtual scrollbar with the real one.
          $wrapper2.scroll(function () {
            $wrapper1.scrollLeft($wrapper2.scrollLeft());
          });
        }

        var anchor = element.find('[data-anchor]'),
          lastWidth = anchor.width(),
          listener;

        // so that when you go to a new link it stops listening
        element.on('remove', function () {
          clearInterval(listener);
        });

        // creates the top virtual scrollbar
        element.wrapInner("<div class='div2' />");
        element.wrapInner("<div class='wrapper2' />");

        // contains the element with a real scrollbar
        element.prepend("<div class='wrapper1'><div class='div1'></div></div>");

        var $wrapper1 = element.find('.wrapper1'),
          $div1 = element.find('.div1'),
          $wrapper2 = element.find('.wrapper2'),
          $div2 = element.find('.div2')

        // force our virtual scrollbar to work the way we want.
        $wrapper1.css({
          width: "100%",
          border: "none 0px rgba(0, 0, 0, 0)",
          overflowX: "scroll",
          overflowY: "hidden",
          height: "20px",
        });

        $div1.css({
          height: "20px",
        });

        $wrapper2.css({
          width: "100%",
          overflowX: "scroll",
        });

        listener = setInterval(function () {
          widthListener();
        }, 650);

        updateScroll();
      }
    }
  }]);

angular.module('app')
  .directive('format', ['$filter', function ($filter) {
    return {
      require: '?ngModel',
      link: function (scope, elem, attrs, ctrl) {
        if (!ctrl) return;

        ctrl.$formatters.unshift(function (a) {
          var format = attrs.format.split(':')[0];
          var symbol = '';
          var filtered = '';
          if (format == 'currency') {
            symbol = (attrs.format.split(':')[1]) || 'Rp.';
            filtered = $filter(attrs.format)(ctrl.$modelValue, symbol);
          } else {
            filtered = $filter(attrs.format)(ctrl.$modelValue);
          };
          // console.log('filtered', filtered, format, symbol, ctrl.$modelValue)
          return filtered
        });

        elem.bind('blur', function (event) {
          var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
          var plainNumberEval = $filter(attrs.format)(plainNumber);
          plainNumberEval = plainNumberEval.replace('$', 'Rp.');
          elem.val(plainNumberEval);
        });
      }
    };
  }]);

angular.module('app')
  .directive("form", function () {
    return {
      require: 'form',
      restrict: 'E',
      link: function (scope, elem, attrs, form) {
        form.doSubmit = function () {
          form.$setSubmitted();
          return scope.$eval(attrs.ngSubmit);
        };
      }
    };
  });