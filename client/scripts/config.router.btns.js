'use strict';

/**
* @ngdoc function
* @name app.config:uiRouter
* @description
* # Config
* Config for the router
*/
angular.module('app')
// mybtns
.config(config)
.run(run);

function config( $stateProvider, $locationProvider, $urlRouterProvider,  MODULE_CONFIG, LoopBackResourceProvider) {
  // Use a custom auth header instead of the default 'Authorization'
  /*LoopBackResourceProvider.setAuthHeader('X-Access-Token');
  // Change the URL where to access the LoopBack REST API server
  LoopBackResourceProvider.setUrlBase('http://localhost:2112/');
  */
  $urlRouterProvider
  .otherwise('/');
  $stateProvider
  .state('btns', {abstract: true, url: '/cargo',views: {
    '': {templateUrl: 'views/layout.html'},
    'aside': {templateUrl: 'views/aside.html'},
    'content': {templateUrl: 'views/content.html'}},
    data: {folded: true}
  }
)  .state('auth', {
    url: '/auth',
    template: '<div class="indigo bg-big"> <div ui-view class="fade-in-down smooth"></div></div>'
  })
  .state('transaction', {
    url: '/',
    templateUrl: 'views/cargo/TransactionList.html',
    controller: 'TransactionListController',
    resolve: load(['scripts/components/TransactionListController.js'])
  })
  .state('transaction_new', {
    url: '/transaction/new',
    templateUrl: 'views/cargo/TransactionForm.html',
    data : { title: 'Tambah Transaksi' },
    controller: 'TransactionNewController',
    resolve: load('scripts/components/TransactionNewController.js')
  })
  .state('transaction_edit', {
    url: '/transaction/:id/edit',
    templateUrl: 'views/cargo/TransactionForm.html',
    data: {
      title: 'Edit Transaksi'
    },
    controller: 'TransactionNewController',
    resolve: load('scripts/components/TransactionNewController.js')
  })
  // .state('auth.signin', {
  //   url: '/signin',
  //   templateUrl: 'views/cargo/signin.html',
  //   controller: 'AuthController',
  //   resolve: load(['scripts/components/AuthController.js'])
  // })
  // .state('auth.logout', {
  //   url: '/logout',
  //   templateUrl: '',
  //   controller: 'LogoutController',
  //   resolve: load(['scripts/components/AuthController.js'])
  // })
  // .state('auth.signup', {
  //   url: '/signup',
  //   templateUrl: 'views/pages/signup.html'
  // })
  // .state('auth.forgot-password', {
  //   url: '/forgot-password',
  //   templateUrl: 'views/pages/forgot-password.html'
  // })
  // .state('btns.dashboard', {
  //   url: '/dashboard',
  //   templateUrl: 'views/cargo/dashboard.html',
  //   data : { title: 'Dashboard'},
  //   authenticate: true,
  //   controller: 'DashboardController',
  //   resolve: load(['scripts/components/DashboardController.js']),
  //   //resolve: load([])
  // })
  // .state('component', {
  //   url: '/component', abstract: true, template: '<div ui-view></div>',
  //   views: {'': {templateUrl: 'views/layout.html'},'aside': {templateUrl: 'views/aside.html'},
  //   'content': {templateUrl: 'views/content.html'}}, data: {folded: true}
  // })
  // .state('component.user', {
  //   url: '/user',
  //   templateUrl: 'views/cargo/User.list.html',
  //   data : { title: 'User', subtitle:'User / List' },
  //   authenticate: true,
  //   controller: 'UserController',
  //   resolve: load('scripts/components/UserController.js')
  // })
  // .state('component.user_new', {
  //   url: '/user/new',
  //   templateUrl: 'views/cargo/User.form.html',
  //   data : { title: 'User', subtitle:'Entry New User' },
  //   authenticate: true,
  //   controller: 'UserNewController',
  //   resolve: load('scripts/components/UserController.js')
  // })
  // .state('component.user_edit', {
  //   url: '/user/:ID/edit',
  //   templateUrl: 'views/cargo/User.form.html',
  //   data : { title: 'User', subtitle:'Edit User' },
  //   authenticate: true,
  //   controller: 'UserEditController',
  //   resolve: load('scripts/components/UserController.js')
  // })
  // .state('component.user_view', {
  //   url: '/user/:id/view',
  //   templateUrl: 'views/cargo/User.view.html',
  //   data : { title: 'Data New User' },
  //   authenticate: true,
  //   controller: 'UserViewController',
  //   resolve: load('scripts/components/UserController.js')
  // })
  // .state('component.region', {
  //   url: '/region',
  //   templateUrl: 'views/cargo/Region.list.html',
  //   data : { title: 'Data Region' },
  //   authenticate: true,
  //   controller: 'RegionController',
  //   resolve: load('scripts/components/RegionController.js')
  // })
  // .state('component.region_new', {
  //   url: '/region/new',
  //   templateUrl: 'views/cargo/Region.new.html',
  //   data : { title: 'New Region' },
  //   authenticate: true,
  //   controller: 'RegionNewController',
  //   resolve: load('scripts/components/RegionController.js')
  // })
  // .state('component.region_edit', {
  //   url: '/region/:id/edit',
  //   templateUrl: 'views/cargo/Region.new.html',
  //   data : { title: 'Edit Region' },
  //   authenticate: true,
  //   controller: 'RegionEditController',
  //   resolve: load('scripts/components/RegionController.js')
  // })
  //     .state('component.menu', {
  //       url: '/menu',
  //       templateUrl: 'views/cargo/menu.list.html',
  //       data: {
  //         title: 'Menu',
  //         subtitle:'Menu / List'
  //       },
  //       authenticate: true,
  //       controller: 'MenuController',
  //       resolve: load('scripts/components/MenuController.js')
  //     }).state('component.menu_new', {
  //       url: '/menu/new',
  //       templateUrl: 'views/cargo/menu.form.html',
  //       data: {
  //         title: 'Entry Menu'
  //       },
  //       authenticate: true,
  //       controller: 'MenuNewController',
  //       resolve: load('scripts/components/MenuController.js')
  //     }).state('component.menu_edit', {
  //       url: '/menu/:id/edit',
  //       templateUrl: 'views/cargo/menu.form.html',
  //       data: {
  //         title: 'Edit Menu'
  //       },
  //       authenticate: true,
  //       controller: 'MenuEditController',
  //       resolve: load('scripts/components/MenuController.js')
  //     }).state('component.role', {
  //       url: '/role',
  //       templateUrl: 'views/cargo/role.list.html',
  //       data: {
  //         title: 'Role',
  //         subtitle : 'Role / List'
  //       },
  //       authenticate: true,
  //       controller: 'RoleController',
  //       resolve: load('scripts/components/RoleController.js')
  //     }).state('component.role_new', {
  //       url: '/role/new',
  //       templateUrl: 'views/cargo/role.form.html',
  //       data: {
  //         title: 'Role', subtitle :'Entry New Role'
  //       },
  //       authenticate: true,
  //       controller: 'RoleNewController',
  //       resolve: load('scripts/components/RoleController.js')
  //     }).state('component.role_edit', {
  //       url: '/role/:id/edit',
  //       templateUrl: 'views/cargo/role.form.html',
  //       data: {
  //         title: 'Role', subtitle:'Edit Role'
  //       },
  //       authenticate: true,
  //       controller: 'RoleEditController',
  //       resolve: load('scripts/components/RoleController.js')
  //     })
  //     .state('component.province', {
  //       url: '/province',
  //       templateUrl: 'views/cargo/Province.list.html',
  //       data : { title: 'Provinsi', subtitle : 'Provinsi / List' },
  //       authenticate: true,
  //       controller: 'ProvinceController',
  //       resolve: load('scripts/components/ProvinceController.js')
  //     })
  //     .state('component.master_parameter', {
  //       url: '/master_parameter',
  //       templateUrl: 'views/cargo/MasterParam.list.html',
  //       data : { title: 'Master Parameter', subtitle : 'Master Parameter / List' },
  //       authenticate: true,
  //       controller: 'MasterParamController',
  //       resolve: load('scripts/components/MasterParamController.js')
  //     })
  //     .state('component.master_parameter_new', {
  //       url: '/master_parameter/new',
  //       templateUrl: 'views/cargo/MasterParam.form.html',
  //       data : { title: 'Master Parameter', subtitle:'Entry Parameter' },
  //       authenticate: true,
  //       controller: 'MasterParamNewController',
  //       resolve: load('scripts/components/MasterParamController.js')
  //     })
  //     .state('component.master_parameter_edit', {
  //       url: '/master_parameter/:id/edit',
  //       templateUrl: 'views/cargo/MasterParam.form.html',
  //       data : { title: 'Master Parameter', subtitle:'Edit Parameter' },
  //       authenticate: true,
  //       controller: 'MasterParamEditController',
  //       resolve: load('scripts/components/MasterParamController.js')
  //     })
  //     .state('component.province_new', {
  //       url: '/province/new',
  //       templateUrl: 'views/cargo/Province.form.html',
  //       data : { title: 'Provinsi', subtitle : 'Entry Provinsi' },
  //       authenticate: true,
  //       controller: 'ProvinceNewController',
  //       resolve: load('scripts/components/ProvinceController.js')
  //     })
  //     .state('component.province_edit', {
  //       url: '/province/:id/edit',
  //       templateUrl: 'views/cargo/Province.form.html',
  //       data : { title: 'Edit Provinsi' },
  //       authenticate: true,
  //       controller: 'ProvinceEditController',
  //       resolve: load('scripts/components/ProvinceController.js')
  //     })
  //     .state('component.regency', {
  //       url: '/regency',
  //       templateUrl: 'views/cargo/Regency.list.html',
  //       data : { title: 'Kabupaten', subtitle:'Kabupaten / List' },
  //       authenticate: true,
  //       controller: 'RegencyController',
  //       resolve: load('scripts/components/RegencyController.js')
  //     })
  //     .state('component.regency_new', {
  //       url: '/regency/new',
  //       templateUrl: 'views/cargo/Regency.form.html',
  //       data : { title: 'Tambah Kabupaten' },
  //       authenticate: true,
  //       controller: 'RegencyNewController',
  //       resolve: load('scripts/components/RegencyController.js')
  //     })
  //     .state('component.regency_edit', {
  //       url: '/regency/:id/edit',
  //       templateUrl: 'views/cargo/Regency.form.html',
  //       data : { title: 'Edit Kabupaten' },
  //       authenticate: true,
  //       controller: 'RegencyEditController',
  //       resolve: load('scripts/components/RegencyController.js')
  //     })
  //     .state('component.customer', {
  //       url: '/customer',
  //       templateUrl: 'views/cargo/CustomerListCustomer.html',
  //       data : {title: 'Data Customer', subtitle:'Customer / List'},
  //       authenticate: true,
  //       controller: 'CustomerListController',
  //       resolve: load('scripts/components/CustomerListController.js')
  //     })
  //     .state('component.customer_new', {
  //       url: '/customer/new',
  //       templateUrl: 'views/cargo/CustomerFormCustomer.html',
  //       data : { title: 'Tambah Customer' },
  //       authenticate: true,
  //       controller: 'CustomerNewController',
  //       resolve: load('scripts/components/CustomerNewController.js')
  //     })
  //     .state('component.customer_edit', {
  //       url: '/customer/:id/edit',
  //       templateUrl: 'views/cargo/CustomerFormCustomer.html',
  //       data: {
  //         title: 'Edit Customer'
  //       },
  //       authenticate: true,
  //       controller: 'CustomerEditController',
  //       resolve: load('scripts/components/CustomerEditController.js')
  //     })
  //     .state('component.district', {
  //       url: '/district',
  //       templateUrl: 'views/cargo/District.list.html',
  //       data : { title: 'Kecamatan',subtitle:'Kecamatan / List' },
  //       authenticate: true,
  //       controller: 'DistrictController',
  //       resolve: load('scripts/components/DistrictController.js')
  //     })
  //     .state('component.district_new', {
  //       url: '/district/new',
  //       templateUrl: 'views/cargo/District.form.html',
  //       data : { title: 'Tambah Kecamatan' },
  //       authenticate: true,
  //       controller: 'DistrictNewController',
  //       resolve: load('scripts/components/DistrictController.js')
  //     })
  //     .state('component.district_edit', {
  //       url: '/district/:id/edit',
  //       templateUrl: 'views/cargo/District.form.html',
  //       data : { title: 'Edit Kecamatan' },
  //       authenticate: true,
  //       controller: 'DistrictEditController',
  //       resolve: load('scripts/components/DistrictController.js')
  //     })
  //     .state('component.subdistrict', {
  //       url: '/subdistrict',
  //       templateUrl: 'views/cargo/SubDistrict.list.html',
  //       data : { title: 'Kelurahan', subtitle:'Kelurahan / List' },
  //       authenticate: true,
  //       controller: 'SubDistrictController',
  //       resolve: load('scripts/components/SubDistrictController.js')
  //     })
  //     .state('component.subdistrict_new', {
  //       url: '/subdistrict/new',
  //       templateUrl: 'views/cargo/SubDistrict.form.html',
  //       data : { title: 'Tambah Kelurahan' },
  //       authenticate: true,
  //       controller: 'SubDistrictNewController',
  //       resolve: load('scripts/components/SubDistrictController.js')
  //     })
  //     .state('component.subdistrict_edit', {
  //       url: '/subdistrict/:id/edit',
  //       templateUrl: 'views/cargo/SubDistrict.form.html',
  //       data : { title: 'Edit Kelurahan' },
  //       authenticate: true,
  //       controller: 'SubDistrictEditController',
  //       resolve: load('scripts/components/SubDistrictController.js')
  //     })

  //   .state('component.fa', {
  //     url: '/fa',
  //     templateUrl: 'views/cargo/Fa.list.html',
  //     data: {
  //       title: 'Financing Application',
  //       subtitle : 'FA / List'
  //     },
  //     authenticate: true,
  //     controller: 'FaListController',
  //     resolve: load('scripts/components/FaListController.js')
  //   })
  //   .state('component.fa_new', {
  //     url: '/fa/new',
  //     templateUrl: 'views/cargo/Fa.form.html',
  //     data: {
  //       title: 'Financing Application',
  //       subtitle : 'Entry Financing Application'
  //     },
  //     authenticate: true,
  //     controller: 'FaNewController',
  //     resolve: load('scripts/components/FaNewController.js')
  //   })
  //   .state('component.fa_edit', {
  //     url: '/fa/:id/edit',
  //     templateUrl: 'views/cargo/Fa.form.html',
  //     data: {
  //       title: 'Financing Application',
  //       subtitle : 'Edit Financing Application '
  //     },
  //     authenticate: true,
  //     controller: 'FaEditController',
  //     resolve: load('scripts/components/FaEditController.js')
  //   })
  //   .state('component.send_task', {
  //     url: '/fa/:id/send_task',
  //     templateUrl: 'views/cargo/Sendtask.form.html',
  //     data : {title: 'Financing Application', subtitle:'Send Task'},
  //     authenticate: true,
  //     controller: 'SendTaskController',
  //     resolve: load('scripts/components/SendTaskController.js')
  //   })
  //   .state('component.fa_lifecycle', {
  //     url: '/fa_lifecycle',
  //     templateUrl: 'views/cargo/Fa.list.html',
  //     data: {
  //       title: 'Financing Application',
  //       subtitle : 'FA Lifecycle '
  //     },
  //     authenticate: true,
  //     controller: 'FaListController',
  //     resolve: load('scripts/components/FaListController.js')
  //   })
  //   .state('component.cust_address_view', {
  //     url: '/cust_address_view',
  //     templateUrl: 'views/cargo/CustomerListCustomer.html',
  //     data : {title: 'Data Customer', subtitle:'Address [View]'},
  //     authenticate: true,
  //     controller: 'CustomerListCustomerController',
  //     resolve: load('scripts/components/CustomerListCustomerController.js')
  //   })
  //   .state('component.role_view', {
  //       url: '/role_view',
  //       templateUrl: 'views/cargo/rbac/role.list.html',
  //       data: {
  //         title: 'Role',
  //         subtitle : 'Role / List [View]'
  //       },
  //       authenticate: true,
  //       controller: 'RoleController',
  //       resolve: load('scripts/components/rbac/RoleController.js')
  //     })
  //   .state('component.menu_view', {
  //       url: '/menu_view',
  //       templateUrl: 'views/cargo/menu.list.html',
  //       data: {
  //         title: 'Menu',
  //         subtitle:'Menu / List'
  //       },
  //       authenticate: true,
  //       controller: 'MenuController',
  //       resolve: load('scripts/components/MenuController.js')
  //     })
  //   .state('component.user_view1', {
  //       url: '/user_view',
  //       templateUrl: 'views/cargo/User.list.html',
  //       data : { title: 'User', subtitle:'User / List' },
  //       authenticate: true,
  //       controller: 'UserController',
  //       resolve: load('scripts/components/UserController.js')
  //     })
  //   .state('component.rm_assigment_view', {
  //     url: '/rm_assigment_view',
  //     templateUrl: 'views/cargo/RMAssigment.list.html',
  //     data : {title: 'RM Assigment [View]', subtitle:'RM Assigment / List [View]'},
  //     authenticate: true,
  //     controller: 'RMAssigmentListController',
  //     resolve: load('scripts/components/RMAssigmentListController.js')
  //   })
  //   .state('component.province_view', {
  //       url: '/province_view',
  //       templateUrl: 'views/cargo/Province.list.html',
  //       data : { title: 'Provinsi', subtitle : 'Provinsi / List View' },
  //       authenticate: true,
  //       controller: 'ProvinceController',
  //       resolve: load('scripts/components/ProvinceController.js')
  //     })
  //   .state('component.regency_view', {
  //       url: '/regency_view',
  //       templateUrl: 'views/cargo/Regency.list.html',
  //       data : { title: 'Kabupaten', subtitle:'Kabupaten / List View' },
  //       authenticate: true,
  //       controller: 'RegencyController',
  //       resolve: load('scripts/components/RegencyController.js')
  //     })
  //   .state('component.district_view', {
  //       url: '/district_view',
  //       templateUrl: 'views/cargo/District.list.html',
  //       data : { title: 'Kecamatan',subtitle:'Kecamatan / List View' },
  //       authenticate: true,
  //       controller: 'DistrictController',
  //       resolve: load('scripts/components/DistrictController.js')
  //     })
  //   .state('component.subdistrict_view', {
  //       url: '/subdistrict_view',
  //       templateUrl: 'views/cargo/SubDistrict.list.html',
  //       data : { title: 'Kelurahan', subtitle:'Kelurahan / List' },
  //       authenticate: true,
  //       controller: 'SubDistrictController',
  //       resolve: load('scripts/components/SubDistrictController.js')
  //     })

  //   .state('component.cabang_list', {
  //     url: '/cabang_list',
  //     templateUrl: 'views/cargo/cabang.list.html',
  //     data: {
  //       title: 'Cabang',
  //       subtitle : 'Cabang'
  //     },
  //     authenticate: true,
  //     controller: 'CabangListController',
  //     resolve: load('scripts/components/CabangController.js')
  //   })
  //    .state('component.cabang_new', {
  //     url: '/cabang_new',
  //     templateUrl: 'views/cargo/cabang.form.html',
  //     data: {
  //       title: 'Cabang',
  //       subtitle : 'Cabang'
  //     },
  //     authenticate: true,
  //     controller: 'CabangNewController',
  //     resolve: load('scripts/components/CabangController.js')
  //   })
  //    .state('component.cabang_edit', {
  //     url: '/cabang/:id/edit',
  //     templateUrl: 'views/cargo/cabang.form.html',
  //     data: {
  //       title: 'Cabang Edit',
  //       subtitle: 'Cabang / Edit'
  //     },
  //     authenticate: true,
  //     controller: 'CabangEditController',
  //     resolve: load('scripts/components/CabangController.js')
  //   })
  //     .state('component.audit_trail', {
  //       url: '/audit_trail',
  //       templateUrl: 'views/cargo/AuditTrail.list.html',
  //       data : { title: 'Audit Trail', subtitle:'Audit Trail / Report' },
  //       authenticate: true,
  //       controller: 'AuditTrailController',
  //       resolve: load('scripts/components/AuditTrailController.js')
  //     })
  //     .state('component.contract', {
  //       url: '/contract',
  //       templateUrl: 'views/cargo/ContractListContract.html',
  //       data : {title: 'Data Contract', subtitle:'Contract / List'},
  //       authenticate: true,
  //       controller: 'ContractListController',
  //       resolve: load('scripts/components/ContractListController.js')
  //     })
  //     .state('component.contract_new', {
  //       url: '/contract/new',
  //       templateUrl: 'views/cargo/ContractFormContract.html',
  //       data : { title: 'Tambah Contract' },
  //       authenticate: true,
  //       controller: 'ContractNewController',
  //       resolve: load('scripts/components/ContractNewController.js')
  //     })
  //     .state('component.contract_edit', {
  //       url: '/contract/:id/edit',
  //       templateUrl: 'views/cargo/ContractFormContract.html',
  //       data: {
  //         title: 'Edit Contract'
  //       },
  //       authenticate: true,
  //       controller: 'ContractEditController',
  //       resolve: load('scripts/components/ContractEditController.js')
  //     })
  //     .state('component.jobsheet', {
  //       url: '/jobsheet',
  //       templateUrl: 'views/cargo/JobsheetListJobsheet.html',
  //       data : {title: 'Data Cargo Order', subtitle:'Cargo Order / List'},
  //       authenticate: true,
  //       controller: 'JobsheetListController',
  //       resolve: load('scripts/components/JobsheetListController.js')
  //     })
  //     .state('component.jobsheet_view', {
  //       url: '/jobsheet_view',
  //       templateUrl: 'views/cargo/JobsheetListJobsheet.html',
  //       data : {title: 'Report Cargo Order', subtitle:'Cargo Order History'},
  //       authenticate: true,
  //       controller: 'JobsheetListController',
  //       resolve: load('scripts/components/JobsheetListController.js')
  //     })
  //     .state('component.jobsheet_new', {
  //       url: '/jobsheet/new',
  //       templateUrl: 'views/cargo/JobsheetFormJobsheet.html',
  //       data : { title: 'Tambah Cargo Order' },
  //       authenticate: true,
  //       controller: 'JobsheetNewController',
  //       resolve: load('scripts/components/JobsheetNewController.js')
  //     })
  //     .state('component.jobsheet_edit', {
  //       url: '/jobsheet/:id/edit',
  //       templateUrl: 'views/cargo/JobsheetFormJobsheet.html',
  //       data: {
  //         title: 'Edit Cargo Order'
  //       },
  //       authenticate: true,
  //       controller: 'JobsheetEditController',
  //       resolve: load('scripts/components/JobsheetEditController.js')
  //     })
  //     .state('component.jobsheet_view_detail', {
  //       url: '/jobsheet/:mode/:id',
  //       templateUrl: 'views/cargo/JobsheetFormJobsheet.html',
  //       data: {
  //         title: 'View Cargo Order'
  //       },
  //       authenticate: true,
  //       controller: 'JobsheetEditController',
  //       resolve: load('scripts/components/JobsheetEditController.js')
  //     })


  //     .state('component.branch', {
  //       url: '/branch',
  //       templateUrl: 'views/cargo/Branch.list.html',
  //       data : { title: 'Cabang', subtitle:'Cabang / List' },
  //       authenticate: true,
  //       controller: 'BranchController',
  //       resolve: load('scripts/components/BranchController.js')
  //     })

  //     .state('component.branch_new', {
  //       url: '/branch/new',
  //       templateUrl: 'views/cargo/Branch.form.html',
  //       data : { title: 'Tambah Cabang' },
  //       authenticate: true,
  //       controller: 'BranchNewController',
  //       resolve: load('scripts/components/BranchController.js')
  //     })
  //     .state('component.branch_edit', {
  //       url: '/branch/:id/edit',
  //       templateUrl: 'views/cargo/Branch.form.html',
  //       data : { title: 'Edit Cabang' },
  //       authenticate: true,
  //       controller: 'BranchEditController',
  //       resolve: load('scripts/components/BranchController.js')
  //     })
  //     .state('component.vendor', {
  //       url: '/vendor',
  //       templateUrl: 'views/cargo/CustomerListCustomer.html',
  //       data : {title: 'Data Vendor', subtitle:'Vendor / List'},
  //       authenticate: true,
  //       controller: 'CustomerListController',
  //       resolve: load('scripts/components/CustomerListController.js')
  //     })
  //     .state('component.vendor_new', {
  //       url: '/vendor/new',
  //       templateUrl: 'views/cargo/CustomerFormCustomer.html',
  //       data : { title: 'Tambah Vendor' },
  //       authenticate: true,
  //       controller: 'CustomerNewController',
  //       resolve: load('scripts/components/CustomerNewController.js')
  //     })
  //     .state('component.vendor_edit', {
  //       url: '/vendor/:id/edit',
  //       templateUrl: 'views/cargo/CustomerFormCustomer.html',
  //       data: {
  //         title: 'Edit Vendor'
  //       },
  //       authenticate: true,
  //       controller: 'CustomerEditController',
  //       resolve: load('scripts/components/CustomerEditController.js')
  //     })
  //     // .state('component.checkout', {
  //     //   url: '/checkout',
  //     //   templateUrl: 'views/cargo/checkout.list.html',
  //     //   data : { title: 'Checkout', subtitle:'Checkout / List' },
  //     //   authenticate: true,
  //     //   controller: 'CheckoutController',
  //     //   resolve: load('scripts/components/CheckoutController.js')
  //     // })

  //     // .state('component.checkout_new', {
  //     //   url: '/checkout/new',
  //     //   templateUrl: 'views/cargo/checkoutForm.html',
  //     //   data : { title: 'Tambah Checkout' },
  //     //   authenticate: true,
  //     //   controller: 'CheckoutNewController',
  //     //   resolve: load('scripts/components/CheckoutNewController.js')
  //     // })
  //     .state('component.checkin', {
  //       url: '/checkin',
  //       templateUrl: 'views/cargo/checkin.list.html',
  //       data : { title: 'Checkin', subtitle:'Checkin / List' },
  //       authenticate: true,
  //       controller: 'CheckinController',
  //       resolve: load('scripts/components/CheckinController.js')
  //     })
  //     .state('component.checkin_new', {
  //       url: '/checkin/new',
  //       templateUrl: 'views/cargo/checkinForm.html',
  //       data : { title: 'Tambah Checkin' },
  //       authenticate: true,
  //       controller: 'CheckinNewController',
  //       resolve: load('scripts/components/CheckinController.js')
  //     })
  //    .state('component.checkout', {
  //       url: '/checkout',
  //       templateUrl: 'views/cargo/checkout.list.html',
  //       data : { title: 'Checkout', subtitle:'Checkout / List' },
  //       authenticate: true,
  //       controller: 'CheckoutController',
  //       resolve: load('scripts/components/CheckoutController.js')
  //     })
  //     .state('component.manifest_view', {
  //       url: '/manifest_view',
  //       templateUrl: 'views/cargo/manifest.list.html',
  //       data : { title: 'Manifest Order', subtitle:'manifest / List' },
  //       authenticate: true,
  //       controller: 'ManifestHistoryController',
  //       resolve: load('scripts/components/ManifestHistoryController.js')
  //     })
  //     .state('component.checkout_new', {
  //       url: '/checkout/new',
  //       templateUrl: 'views/cargo/checkoutForm.html',
  //       data : { title: 'Tambah Checkout' },
  //       authenticate: true,
  //       controller: 'CheckoutNewController',
  //       resolve: load('scripts/components/CheckoutNewController.js')
  //     })
  //     .state('component.checkout_edit', {
  //       url: '/checkout/:id/edit',
  //       templateUrl: 'views/cargo/checkoutForm.html',
  //       data : { title: 'Tambah Checkout' },
  //       authenticate: true,
  //       controller: 'CheckoutNewController',
  //       resolve: load('scripts/components/CheckoutNewController.js')
  //     })
  //     .state('component.checkout_print', {
  //       url: '/checkout/:id/print',
  //       templateUrl: 'views/cargo/checkoutPrint.html',
  //       data : { title: 'Checkout Print', subtitle:'Checkout Print View' },
  //       authenticate: true,
  //       controller: 'CheckoutPrintController',
  //       resolve: load('scripts/components/CheckoutPrintController.js')
  //     })
  //     // .state('component.trucking', {
  //     //   url: '/trucking',
  //     //   templateUrl: 'views/cargo/schedule.list.html',
  //     //   data : { title: 'Trucking Schedule', subtitle:'Schedule / List' },
  //     //   authenticate: true,
  //     //   controller: 'TruckingController',
  //     //   resolve: load('scripts/components/TruckingController.js')
  //     // })
  //     // .state('component.schedule_new', {
  //     //   url: '/schedule/new',
  //     //   templateUrl: 'views/cargo/scheduleForm.html',
  //     //   data : { title: 'New Schedule' },
  //     //   authenticate: true,
  //     //   controller: 'ScheduleNewController',
  //     //   resolve: load('scripts/components/ScheduleNewController.js')
  //     // })

  //     // .state('component.trucking_new', {
  //     //   url: '/trucking/new',
  //     //   templateUrl: 'views/cargo/truckingForm.html',
  //     //   data : { title: 'New Truck' },
  //     //   authenticate: true,
  //     //   controller: 'TruckingNewController',
  //     //   resolve: load('scripts/components/TruckingNewController.js')
  //     // })
  //     .state('component.trucking', {
  //       url: '/trucking',
  //       templateUrl: 'views/cargo/schedule.list.html',
  //       data : { title: 'Trucking & Schedule', subtitle:'Schedule / List' },
  //       authenticate: true,
  //       controller: 'TruckingController',
  //       resolve: load('scripts/components/TruckingController.js')
  //     })

  //     .state('component.schedule_new', {
  //       url: '/schedule/new',
  //       templateUrl: 'views/cargo/scheduleForm.html',
  //       data : { title: 'New Schedule' },
  //       authenticate: true,
  //       controller: 'ScheduleNewController',
  //       resolve: load('scripts/components/ScheduleNewController.js')
  //     })
  //     .state('component.schedule_edit', {
  //       url: '/schedule/:id/edit',
  //       templateUrl: 'views/cargo/scheduleForm.html',
  //       data: {
  //         title: 'Edit Schedule'
  //       },
  //       authenticate: true,
  //       controller: 'CustomerEditController',
  //       resolve: load('scripts/components/ScheduleEditController.js')
  //     })
  //     .state('component.trucking_new', {
  //       url: '/trucking/new',
  //       templateUrl: 'views/cargo/truckingForm.html',
  //       data : { title: 'New Truck' },
  //       authenticate: true,
  //       controller: 'TruckingNewController',
  //       resolve: load('scripts/components/TruckingNewController.js')
  //     })
  //     .state('component.trucking_edit', {
  //       url: '/trucking/:id/edit',
  //       templateUrl: 'views/cargo/truckingForm.html',
  //       data: {
  //         title: 'Edit Trucking'
  //       },
  //       authenticate: true,
  //       controller: 'TruckingEditController',
  //       resolve: load('scripts/components/TruckingEditController.js')
  //     })
  //     .state('component.cekongkir', {
  //       url: '/cekongkir',
  //       templateUrl: 'views/cargo/CekOngkirLIst.html',
  //       data : {title: 'Cek Ongkir', subtitle:'CekOngkir / List'},
  //       authenticate: true,
  //       controller: 'CekOngkirListController',
  //       resolve: load('scripts/components/CekOngkirListController.js')
  //     })
  //     .state('component.cekongkir_new', {
  //       url: '/cekongkir/new',
  //       templateUrl: 'views/cargo/CekOngkirForm.html',
  //       data : { title: 'Cek Ongkir' },
  //       authenticate: true,
  //       controller: 'CekOngkirNewController',
  //       resolve: load('scripts/components/CekOngkirNewController.js')
  //     })
  //     .state('component.cekongkir_edit', {
  //       url: '/cekongkir/:id/edit',
  //       templateUrl: 'views/cargo/CekOngkirForm.html',
  //       data: {
  //         title: 'Edit Cek Ongkir'
  //       },
  //       authenticate: true,
  //       controller: 'CekOngkirEditController',
  //       resolve: load('scripts/components/CekOngkirEditController.js')
  //     })
  //     .state('component.service', {
  //       url: '/service',
  //       templateUrl: 'views/cargo/ServiceLIst.html',
  //       data : {title: 'Komplain', subtitle:'Komplain / List'},
  //       authenticate: true,
  //       controller: 'ServiceListController',
  //       resolve: load('scripts/components/ServiceListController.js')
  //     })
  //     .state('component.service_new', {
  //       url: '/service/new',
  //       templateUrl: 'views/cargo/ServiceForm.html',
  //       data : { title: 'Tambah Komplain', subtitle:'Tambah/ Komplain' },
  //       authenticate: true,
  //       controller: 'ServiceNewController',
  //       resolve: load('scripts/components/ServiceNewController.js')
  //     })
  //     .state('component.service_edit', {
  //       url: '/service/:id/edit',
  //       templateUrl: 'views/cargo/ServiceForm.html',
  //       data: {
  //         title: 'Edit Komplain'
  //       },
  //       authenticate: true,
  //       controller: 'ServiceEditController',
  //       resolve: load('scripts/components/ServiceEditController.js')
  //     })
  //     .state('component.manifest_item_view', {
  //       url: '/manifest_item_view',
  //       templateUrl: 'views/cargo/manifestItem.list.html',
  //       data : { title: 'Manifest Item', subtitle:'manifest Item / List' },
  //       authenticate: true,
  //       controller: 'ManifestItemHistoryController',
  //       resolve: load('scripts/components/ManifestItemHistoryController.js')
  //     })
  //     .state('component.route_cargo', {
  //       url: '/route_cargo',
  //       templateUrl: 'views/cargo/RouteCargoList.html',
  //       data : {title: 'Data Rute Cargo', subtitle:'Rute Cargo / List'},
  //       authenticate: true,
  //       controller: 'RouteCargoListController',
  //       resolve: load('scripts/components/RouteCargoListController.js')
  //     })
  //     .state('component.route_cargo_new', {
  //       url: '/route_cargo/new',
  //       templateUrl: 'views/cargo/RouteCargoForm.html',
  //       data : { title: 'Tambah Rute Cargo' },
  //       authenticate: true,
  //       controller: 'RouteCargoNewController',
  //       resolve: load('scripts/components/RouteCargoNewController.js')
  //     })
      // .state('component.contract_edit', {
      //   url: '/contract/:id/edit',
      //   templateUrl: 'views/cargo/ContractFormContract.html',
      //   data: {
      //     title: 'Edit Contract'
      //   },
      //   authenticate: true,
      //   controller: 'ContractEditController',
      //   resolve: load('scripts/components/ContractEditController.js')
      // })
     
  ;
  // use the HTML5 History API
  // $locationProvider.html5Mode(true);
  function load(srcs, callback) {
    return {
      deps: ['$ocLazyLoad', '$q',
      function( $ocLazyLoad, $q ){
        if (angular.isArray(srcs))
          return $ocLazyLoad.load({files: srcs});
        else
          return $ocLazyLoad.load(srcs);
        /*var deferred = $q.defer();
        var promise  = false;
        srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
        if(!promise){
          promise = deferred.promise;
        }
        angular.forEach(srcs, function(src) {
          promise = promise.then( function(){
            angular.forEach(MODULE_CONFIG, function(module) {
              if( module.name == src){
                if(!module.module){
                  name = module.files;
                }else{
                  name = module.name;
                }
              }else{
                name = src;
              }
            });
            return $ocLazyLoad.load(name);
          } );
        });
        deferred.resolve();
        return callback ? promise.then(function(){ return callback(); }) : promise;*/
      }]
    }
  }
}

function run( $rootScope, $state, $stateParams,Utils, LoopBackAuth, User, $localStorage, Menu,$timeout, Acl) {
  $rootScope.appMenus = [];
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  // Aplikasi.copyAplikasiAndRelation({old_id : 8, new_id: 181}, function onSuccess(res){
  //   console.log("copy status", res.status);
  // })
  $rootScope.$on('$stateChangeStart', function(event, next) {
    if (next.name.indexOf("auth.") == 0) {
      $localStorage["appMenus"] = null
    }

    if($rootScope.currentMenu == null)
      $rootScope.currentMenu =  {name : "btns.dashboard"};
    if(LoopBackAuth.currentUserId && next.name != "btns.dashboard" && next.name.indexOf("auth.") != 0){
      let url = "component/" + next.url.split("/")[1];
      // allow access menu without chek
      //let available =  getParentFromListMenuUrl($localStorage["appMenus"],url)
      // if(!available){
      //   event.preventDefault();
      //   $state.go($rootScope.currentMenu.name)
      // }
    }

      if(next.authenticate && !LoopBackAuth.accessTokenId){
        event.preventDefault(); //prevent current page from loading
        // $state.go('transaction');
      }
      if ($localStorage[LoopBackAuth.accessTokenId] && !$rootScope.currentUser) {
        $rootScope.currentUser = $localStorage[LoopBackAuth.accessTokenId]
        $rootScope.currentBranch = $localStorage['currentBranch'];
      };
      if ($localStorage['appMenus'] && $localStorage[LoopBackAuth.accessTokenId]) {
        $rootScope.appMenus = $localStorage['appMenus'];
      } else {
        let currUser = $localStorage[LoopBackAuth.accessTokenId];
        if( next.name.indexOf("auth.") != 0 && currUser){
          Menu.getUserMenu({
            user_id: currUser.ID
          }, function (data) {
            $localStorage['appMenus'] = data;
            $rootScope.appMenus = data;
            return;
          })
        }

      }
    });

  $rootScope.setActiveMenu = function(menu){
    $rootScope.currentMenu = $rootScope.$state.current;
    let selectedMenu = getParentFromForest($rootScope.appMenus,menu.id)
    selectedMenu.activeMenu =  selectedMenu.activeMenu ? false : true;
  }

  function getParentFromListMenuUrl(forest, url){
    let result = null;
    for(let el of forest){
      if(el.url == url)   result = el;
      if(el.subMenu)    result = getParentFromUrl(el, url);
      if(result)        return result
    }
    return result
  }

  function getParentFromUrl(tree,url){
    let result = null;
    if(tree.url == url)
        return tree;
    if(tree.subMenu){
        for(let el of tree.subMenu){
          if(el.url == url)   return el;
          if(el.subMenu)    result =  getParentFromListMenuUrl(el.subMenu, url)
          if(result)        return result
        }
    }
    return null;
  }


  function getParentFromForest(forest, id){
    let result = null;
    for(let el of forest){
      if(el.id == id)   result = el;
      if(el.subMenu)    result = getParentFromTree(el, id);
      if(result)        return result
    }
    return result
  }

  function getParentFromTree(tree,id){
    let result = null;
    if(tree.id == id)
        return tree;
    if(tree.subMenu){
        for(let el of tree.subMenu){
          if(el.id == id)   return el;
          if(el.subMenu)    result =  getParentFromForest(el.subMenu, id)
          if(result)        return result
        }
    }
    return null;
  }

  $rootScope.appModule = [
    {name: 'Acl' ,model :'acl'},
    {name: 'Apps Menu' ,model :'Menu'},
    {name: 'Menu Model' ,model :'MenuModel'},
    {name: 'Master Parameter' ,model :'MasterGeneric'}
  ];

  $rootScope.aclAccessType = [
    {label:'WRITE',value:'WRITE'},
    {label:'READ',value:'READ'},
    {label: 'MENU',value: 'MENU'},
  ];

}
