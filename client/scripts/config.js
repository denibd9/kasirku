// config

// var app =  
// angular.module('app')
var app = angular.module('app')
  .config(
    [        '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
    }
  ])
  .config(['$translateProvider', function($translateProvider){
    // Register a loader for the static files
    // So, the module will search missing translation tables under the specified urls.
    // Those urls are [prefix][langKey][suffix].
    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.js'
    });
    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
  }])
  .config(['$httpProvider', function($httpProvider){
    $httpProvider.interceptors.push(function($q, LoopBackAuth, $location) {
      return {
        responseError: function(rejection) {
          console.log('on interceptors responseError faced: ', rejection);
          if (rejection.status == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/auth/signin');
            // $state.go('auth.signin');
          }
          if (rejection.status == 500 && (rejection.data && rejection.data.error && rejection.data.error.message == "Invalid Access Token")) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/auth/signin');
            // $state.go('auth.signin');
          }
          return $q.reject(rejection);
        }
      }; 
    });
  }])
  // directive('siteHeader',sideHeader);

  /*return {
    restrict: 'E',
    template: '<button class="btn">{{back}}</button><button class="btn">{{forward}}</button>',
    scope: {
      back: '@back',
      forward: '@forward',
      icons: '@icons'
    },
    link: function(scope, element, attrs) {
      $(element[0]).on('click', function() {
        history.back();
        scope.$apply();
      });
      $(element[1]).on('click', function() {
        history.forward();
        scope.$apply();
      });
    }
  };*/
