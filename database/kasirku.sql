-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for kasirku
CREATE DATABASE IF NOT EXISTS `kasirku` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kasirku`;

-- Dumping structure for table kasirku.accesstoken
CREATE TABLE IF NOT EXISTS `accesstoken` (
  `id` varchar(255) DEFAULT NULL,
  `ttl` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `userId` varchar(100) DEFAULT NULL,
  `scopes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.accesstoken: ~139 rows (approximately)
/*!40000 ALTER TABLE `accesstoken` DISABLE KEYS */;
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rc1mjbaRVSGq8LWEssUDf5o09Svi1BkQM7hgIEY77fB7xKSsYCstOVL0lXoIhf8i', '1209600', '2021-07-21', '1', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('oEeFnuKiCM0MeCIQWSFq8vCjivf539MkQoud4YmG6mx3cZ7AttDM5MUIQWv40pNW', '1209600', '2021-07-21', '1', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('iISARyybAi0cuFaHu2WhsknV3YCBoXhKtECWdy41Scy6BLegJ5emk55Hi1zbzFvy', '1209600', '2021-07-21', '1', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('7HI0EFwyWDfuVFnVP0NzDdoFt7tau74zV1Gecim3ZDgJDz0sabIzuh1KYBzIkPW2', '1209600', '2021-07-21', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ayhNA5zsp4bePJ55HyO4rxeS7SDE7MP234HoaMVox1SyGSSToI5ZnQeQSLpKarbc', '1209600', '2021-07-21', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('0UYBTrFh3hHqcAvrCwIBmPpXFQYxk8eGB5M0zLKGdfF2HnWtHPRU81jzvipk6k6g', '1209600', '2021-07-21', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('U3DIGqFTXgh4hdjdyeon8VbdBWnW4nOHVXHYR163SdOFDCojaAuF584DpA91FgXi', '1209600', '2021-07-29', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('tSNMIGvlMwNqd3zD98wYjWprtV7g7BiLn3sHKPN2CPGbkZP9lk3KCd0hohZ6fIYA', '1209600', '2021-08-02', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rOsJuGVxxNkyIoPA7p0SLcUGAzWGUyQhUNmRpcsYLJe7doprElegEPXiNOse8Av5', '1209600', '2021-08-02', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('GNIX31I5g50WoDy1P0xKxglXAHdRRE9gcFN2S4TWlkEu37UpCeCWo39RTCoMbL7t', '1209600', '2021-08-02', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('maQShQSweKRWe01LGGbfa0h6lHadOIzn8PLjppmDyNvCzi9tauFcAEEl9OMcv6kH', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('oey7QqOFXrqx8R0j5H8WR3yD5yvau2scj7WWExtshiiv9L3YQUm7Extn3k0DgLQF', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('T5krFCuF2soDSsuFbLfGUIB9ECBIyCBP89GdT6f8cI45Epc8Cgw85QXWe8USQB23', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('MxrEcSX5in6NcBF363nkA1kmzjguLDFThXASOR2OibWBE6Hj3CnKPdgXYvEy7YDg', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('EWBxrFnEWa3HfwjcIVdW9Dv6BMGXwWHmzZHoakkdLgXbiSjXHXqE0oPv7WeYrZiJ', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('PKBBQrlhVsBMCb5RAI7QiLiVPzb47A93YosCksmTOKfVIeR03qfstAjIum4uEBez', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rzy3FJqyGUBf2YET6kdoOYx5NYxPMYnRQINoAB7BHi1tmdbGhesEX0sFANrVDHIf', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('sgQT550ZYyvC2PimLd7sE2AZ4JedkTVokOpYHzZl112Tr7jOa6si9TUjVwd4zAai', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('1STEGoYwz9YjD7gnymqETJUNUuC7tFxCOBmpkMU4LaOOfwAgL8hO4ixDjdeGX1uG', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('qDja9084RHqh073sWtMj9EIfQdsCloAKf47QJSObmHdKb5S3NepKkUdlHlr9QcPQ', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('YjobvS14vEZZvGTW74IPWWbhtah49THW4Z2KJSC0jL2H8ljOetzXs7SIzfxJS6Zx', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Id5BRHASzWaGDzKQFGKzpHCGXs5R7tEBz7eZiKdg1QnnP2IkcF1dQjZMYaibxKjJ', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('fzdR1IAABu4BhHYhjV9QmbyM9ETpqGghlp5mMsYxtslADGS3vDt6sSf7HJZZ3a9c', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('NesegviBxMH22vnr5PC4njiCjpNwuuUKGMe8xsjoNwJ5HDRrf8EwPI0QQFWyRXAl', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('glsEFNfJFVH7EK9304KZgh1ISQdlOSAoPXwatb6M9rDoS6682C8Fcp6wpzfLY3eS', '1209600', '2021-08-04', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('FuJz1nyzLFbLaiBsy4LcjRO5AiQNcFGOklt8ZFFPDKm7AEa86ZFsSc015xYpSZwK', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('JwuxyqZJugRD2dT7SAiOFfuqbmSf2OuMSwJsdoSzA7h7uYBkfaA2cvHgo1faBju2', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('vQPDKMAgHbuByPNzGLpYBSdnrLxSsKFwbQSeD8F0kAeDzb9rFy1CSaLpLBIY7erw', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('CM6xuVXciESWbGdFkC6yCqv9s5CkhYO1G7H76AWRdQ3FcZAY7LvxMmzmiUTroGeU', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rPjthxhtMQCHqwgtiCsalzzsBvS1ThG9Pf1fAUPY0kv8oRBRzD4YOi8vJxkmonf9', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Al5tMeRHfkkpReW4SGCg1nQr25hA7FEXaQmT6TziBy9N0IjxONzsLFxflFlzRKQr', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rNwp0M8zeHIppEqbkfy3xoMIU1SD2kbj1BFD9DWhlrvhpNZz8NBVbg6TgDwUMFRc', '1209600', '2021-08-05', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('u7LMiyB0azAAnIhnA9lKVPJweqEeXW7DQCZzzeIWv7Tbc0qCsITzsKEMcRC0AJGE', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ep1mcQarKVlQZJeiTU1R9UoYiEvT1je3ixGiw2JoLuqWrm8Abbfh2U5HcyHrRYWF', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('XBTxuTl23e5nhDvm3T15BcjyZUzF09Fdt45GbHfvKZAULGj5N4W87RlQOkEuBHBs', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('hWljGLkfYi9QWVn40OdlFxrI7lrR2sRfPIPtzUShS7TFfW3Wsmu9lPoS3lXBMqYK', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('rXd5WPo5ctDjRnshdZoWgyF4Wq7PwgT0GqtlUBYzcGCF9K1EBw86IqmAeeOeKrKs', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ZsgXVp8TtfoAFQJLAz4gDtdpvBDkRDOvXfwKGNi6GMIP2GUdSAA4X8Q4bHx4K65P', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('9lgGSGYFK3dWIo8hgtCWKBbAWHjcmWQ48RWZBMekgQhWmpsuaGE079T594D6NCf0', '1209600', '2021-08-06', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('DQqDI3ijAdPLz0RXXyJXrhmeVx3u1IToXezBSUgQ63S8yy5r705bNCTFNsFh70jC', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('fYcrsH8wLfloFHxKgRP6IetC5RfJKPNACNHOhnJRF5YnXC2KX8JJir8fmq0KKrpI', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('GuDO6bDD9bGP0UHV6Ch8fD8nPs0FhpZlfl0RnWMqbgYPChCQ3UOywonwTosAFlGC', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('hRti15JXY1Csr7IRAFzLBrrSIAAdEaEA6kUUWNSsSN5fhYiz1rIz4kVNpsQRHaEQ', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('AoYPHadsPFMDxFkFEMfvWt6vR7qgsdmKZfnjSxk34FkaqJ4NPUrJQVDkvTgkYeHN', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Md79R3SgOSDfyyIflVLPAyeqT5FgcCWoFV00qVbdBcRY2OhMMYYPmomp7jx8yq4H', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('RvYj2fUdk4P0nsVRgd7iZLQVreIQDTyEZ97RdAHbxAatPGNETFs6Hm19z7Hf9iSc', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('8VFlMHuVVTGTbQuLEwDiFvQlbAHVor2WhBLNVKuzeoJhZs09BV9X03yBWCTsctip', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('7oDYMtuzEu9P6zkhHjVCCmofBu8c8SKVSEKQ5S8csnERMpa56N6GGIcfXB1JDN9C', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('z1gPM9iitlnvMvDzOiFgrM1VNS7vSiKztCD0FAtWm11nlTvmvq067bTvOyvubzNe', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('BnXsELa4GdtCdCv6oK21bluogvQXz9Nc0Ot4c1703QJ5bACuLiGMVGFCRtBBdA8M', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ucMd8MujTebYLBX1xxrhl1euclpeQbXxLAouHM6w2pe6U1u65rfE40TwryiylLBs', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('78qLkcxORXGrYzFYChal6wF4rZF0UNlXevzDxqKzcXyg5HX06zvJPCnVo1Id3mvV', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('sLiy5EoEgrNxQ1Ba3ZyWekl9WXM1iTUtvCFUZNUctCoJrEoeqpfwVPAoxGeRkEoF', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('CYrlApbAeVJOMvBwRe4l1SmbAuVBXmCGEONuiZqm7Fw8WsykOw33G7Ld9ys0JwNC', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('xIiKr9bBAOyLE1eIuV1mEyKgAecs3C1FCMT0bdfwHZmAArDipqry8loCBGEM68K1', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('kueEp7fu2gw3Qp26OMaEESkXqY2lGERmhsmmPuIsiViulFu5yT7XauCaP5Bq1tY7', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('MsWT8q3DEMBYInKjWCNWTWJO3E7cfnHRdJrCAwD2wTQAwjUDeytG75F8xQKDFTRP', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('kBPQ1BeIGvzKspIa2oEH9o2Fhle7j8cryAS1H20yQAy7Wt9lqCzl4zD33iyPaUrT', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ByO3TC6aZ66uWV7XHvnFQlehSfkA7zX0GCJiSZzeyTRexPEcZyDYyRK3F0MTJXFL', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('H6fKTYd58lzwCRIVvZzAmQ59qjwSF3rvUL2qJ3fSHyOlwYzMDzmrRDmpzc90wMmt', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('x8kou5hIY7JhZn6c4gdJDCwDdDD5WLK9VRxbcE9wrwcFJ3UMfqsRx8Hei7Jfmw7J', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('FGETaCRpgsgIofAc2Oi1Gt0hFhiuwwzxvVRLLXeqIQktRaBMpLZDO1jjJTnWGzuB', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Hlg5eNNctXFGB62TMjue0rm34Vsrvzcl064P7epBb6CsLaXpF7GsdKCYXk0PnwGV', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('tjB9sIkIGDxrTHZFPrkmPyyxT5nRumEV3A9iDJ8iWH1xQF4ZJ0lMRMLYNFNDF9mm', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('GfoHqSyzA30zXfO8YnfKFC7Mt4LmsPLSK4MqO1DE3CCXq9k5Flc5OmPPZjH2aRm4', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('D78MDyaRqZMp2bSSJ1VJF3lv7ah9HjuAvJaOGVMoaYxKHPtLHLU3M3ggZRTMwIW2', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('exq8jyNjIxStYQ5hve3Q43rPMBuyynxxxBydqCCCyJRSPapUHVyyGTaFVAABsqVy', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('JuHnN9xYkpobuppKmsVpLSNE7q4a9EEjmNqIA6esLEjBfjZ7v7Qx6esgAOBkAFRd', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Bksxm8LaPG4H7mjdVNxQkrPQOPNYC0W0fzoNDvhuYTQ1Iwq1L1kzFBAEKuwWzpfH', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('cVk92umrWfE7Ujf5wCEUANfSSG5qazEkll5NDTtkD4TxwxiE3KEltnqb8Xk4Zjod', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('kWIY39m8MXFp6ViulmfJ00uS9k60DY8m6Fu71REJsp0e9DW4enuRV9oSrQXzGqYU', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('nwRBvGy3vK7N46FrEH5Sl4jZqfz24jQ7tBSrlKT4JcZ9tj4AQN88BGEEBSMKr4Jm', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('p6SU4bO2LAyOLhYNp5gdeOVyhnZEJkgGSiPAMM8SnjaeS9O5vcTOMAbMxTuNdJPO', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('prWh1CSKHjnmPghHzENSimxfGBWaUyhg6zKAWcue6VOOIMFdT24YjsUAIWrhx7eH', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('5jiZfvzJtGldzPeTxt6Zvfjq47qY09cRUEdLhlEzzBaymmbwEZuTC25NAKDX82Y4', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('inJ78tNzkCPqRkuy03cr5OWBTtDgSJTCHSouva2QgrXC7xdzFvQSyppQnkmSMhLs', '1209600', '2021-08-09', 'rashad', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('wuXB1aEAdFDKDyCw260uFSUF4QqXl1q2Q4pGFSs7ozCOEgR2T1I0OW6C42ZaJpij', '1209600', '2021-08-09', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('zhm3Zh5ZaVxD3rFkAXyG2UVOhwUvKOeyhNlZ5n6Vdz1i9nPIldWC6FClcp2SpgIT', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('4ZQUHiMkMC3U4dTvYuLSThCA8CTPvsQrfOdbOsir2OV9Ky1iHsCiSSbXV1PULkDD', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('yyvB2Bb8PsDoLeEjKbDDWfsPtdnzah41Y09Rdv0pGzo3spBCtUnCRALVRD6FfOS8', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('XBGpdp36PucEl8aT6CdGyspFbRq6PqalDk9VCAcHal9jNL8JFO15bFd8MjQYUZdY', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('GiXRBHRfo87PSFHmfQ54H5qwuk2YjFCfcrLTNk0J3JQ0FWKW729Bh9LrRxhcix2f', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('hJ4PGh6oVHAXyV2f8wwre4O5QGBFtoue9x4D8PXi9nkDByB3q4MLyFwstvyzYp0v', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('5GzgTpf40ZhITrpspuEWyThbzCGqBNmVTbrsDhSOXGstxmNTJGhZGmndDz0tmokp', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('FaaM7wHhyHvIxAFO2sI9vL4Jxf2HmIBtv1mGwSZbHXpERjmQeFAHtCFSMZzntAVp', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('w8PPYCYcv2ZP0o00v96IgXMv9a9HUMeC5KokN3eeAcyxeaq6XHtCfHhGwK8XERnc', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('at5cT1aNwg6yA1yQoX3TU5pKKpoaIpm8VlkAL6MDy0OqZv8uoOePJoTpVAbWj59D', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('SnNGWt3xyn9vq37keiYgkd9vrQk63G1jrHWCFG2pYDNCplVPycGUlSHmDAfYUFjA', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('CHAw64oqRYE9HlUKWyJh5EBqt0jHNTaqdUEDH0KdDWMPIT1mas6jw8qECDSVUk23', '1209600', '2021-08-10', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('Zj2UaL6X1FhBDHhkxrUKHAmNvsWa1STM1fjWWVXUemtanU9D4rfWgo13mmyrvWgp', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('SU9d0Mru8zibaxWLIyq0jXHMG6mr86vDow86sxnrkF1BhOtauiXiNRduNPlALHwC', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('cYH9Bt7nM1hq4kNlOlpvj17pXV1keHvLOMqwIY183XDtaD5mdDLbrTofcXxElBBk', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('mEdAHynzaktU1FB6VwJNgO2v5HEcUlca3bfeR7AbtyHtjY9fMEg0Wj5FMknNoEjP', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('kpHPyGIh58hXMAxSdVfV71j2zKgsM9h84v5TfVCTUZxXjldcpjJn8Q6lvOEw4iqA', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('F6gS3b6BCTaSD58DYELbaXRItSH01lqywif2VJuYA2jtoCuA1lR1K3ytiCjlLGDV', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('KIBaEL0pbR4EevgnbjEmFhy9jo5VwJqoKb2h4XNofXngsDYXSJ5CAUThG3orLnNG', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('pgw5aADSteI1vpTRnFpMEveCp8NOblE9ltfBduZHDk2V0GIVKjfFfhl6SUCEXJFD', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('TrEjN3HbcdLDZTMog3kNq28ZjfJCHY1FEHC5Ej2HdHuFkxpteLiYOrGCvo50IxHc', '1209600', '2021-08-12', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('E3BMDaqSYZBGOFR16ysbcIyJPLZpn87Dcnc6KOWadLk8slxm4BinGdbL2mJ1RSWy', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('P6Lg0xOXS7a58CDDQwzN5hkJ8CPRU55SVOb93KHs3TVBtgpWpLpcLOpBft1a3zA0', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('F7IFSukJbGfwdnyPVsXfTx5niqpkYr3HwqIDsSrJOpLDQpkSTVe9fE1f1jnlB48t', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ZDH3BV0sPZzbHbg7KyWjGL81pECQCUZfiIHulRKUwDgIAERAlGFmcXwP5kt0SCNE', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('dAFsCejCzQ6553Ie40sVK9Zg9M7Fl1JBhvZKy3tqMO8ScbCnshDU49kR4lcjTrpD', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('UrO9HRViIgB8u9bjsUAINnVHg3TDife7DdPPExALolU9bygfVJ2C4iOpAzrDTaX2', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('gZ1xmymfxGPPBrByaVM1rpoaxPorOqjhuWeJ59K87ANgsXHZ9KeezNELeiLQaEGv', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('HSlTikm0fY2Of5DoGE4VHrG3p7QW0RqF9mTKbcFUuEOLhUTLNOWGSRNtzYLcMrTq', '1209600', '2021-08-13', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('QNAJgBLYdSawhVbNgLUv1SXkgrKOiiT7V9d8A7wMLVP11qdBXh0y9DCuiXehgbST', '1209600', '2021-08-16', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('NyRPDMe9qmUg38yKKgtMAYqeJfSEIdKlAzz4nEk93gMuCFAIIh7HG6Y8FFHviKuG', '1209600', '2021-08-16', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('YH6rfBjU1PXOESSiXZal1IWh09iBRElE6jzOOaukyq0GdiMY31FYfeOIY7QE9ul7', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('mbY88EpEM8lOWORR3p9lurzwuQ0GAvgta61f2iVm8a5snQOKbWdGoKfO17ysj45u', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ay6q0P3FbPPFb7RoaNrwiM1K1M6liuN91HFU1eygpErUtaCdJG0oc2wgPLyubw0e', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('DdeueGkNURhWIeLyGYCCOeQPl31YuxID0RHJzdAHgu7dwfVIZXmna5YI3nDaCFGk', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('MVCa4Y1nDedj1Gv5TAfzDDMUDRp1e5TCyDJo0KGQr9Dpc7Dn0s25MMJkWiPGCqzZ', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('1JMv9MwDaYivElWgvvwB7Xh7GVc0vUOqZsSHZdj19BR5Cz2iluK40gQxTg5BokLs', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('JZDBRcgaXPJQnX4EQFMzFFx2PuMQ2boiEWavrEAkpzJgRTRIRxAEN9H6H1W06BYY', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('vnCgCbdHsqBwGVDekkrrNaEdLPaGnJ31LehG2g14jsFTD6eRr5LYuR6lKQLlw76y', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('wqJnyemQClLEms1yFEdMGXTbauQJvapNJIMe5dHicUA7VZjpJkrAO7s9zNMeiWNC', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('GM4RnWYvX9veuQ83mJp2kNkic5bRpAoUN3jmWfWVxJqka5dkYPYUfVNxqFc3oZ1N', '1209600', '2021-08-18', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('49i8EWJXO9OMZ4BOYdMcCY1arVFy6jc2XDi2PiBuNmXi0D3GPiPCdQe8E69SMdaP', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('ceqoeGIKablEBjNjkS0tLM3bn0wRVDdpmiz6XkviYkwHwXdTq81HwNLDICEDucRD', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('De5Xe7NCKcJyh06m3UBXZMMNqRFq7oMmplZD525xdF9oRakgptASEQ2iOWVUSLv0', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('8NU9DaRB4cFiwesIbDUVtDsvBQ1JEIidq5dCMOcyFAVDmtDNaqz9YDKMa31H0r0K', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('MqLIgAVjL2BWLslHLXdrxYw371n9SntqzgXyCvH3PrbykTCY78ZCxMtSMDyfZvGy', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('mxcaAjfkXDdVkHCaPhEvhFByvsOYBcJqYxbl8hBGJ8UuE5yOXaPYJL8ehqjZxOLN', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('O9PRFsRS665LFgdBMb5disAjY69mq0BKVzC4wvX7zKXqd0IK51ly6uHafV8cITf3', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('zCdFq2C1YPyo0svv3BvaH4k7ZpLJmJgBt47bPw9A4U0bmkD6Fl2BUVBD4YbTN2ml', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('fbFfo8Xqgfb0GDiRrRBi0q4iPGwuatQVD4MVKJQhdTkyWMo9sBYnvhnvGz8ab9dB', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('MO6HnGEwwKySexXj3bA2B7TgFjFLzIApGnXGGD5lUAgkfE6w1Tv9MKapvYXW79T6', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('LneRSRRvlIAxGVQ1WUP9SyoD8pCSkLt1FjUCquxThHCd3QfRtBLFdlOhjFCKGMv3', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('frMDzmIYHFantIytrYLs4l5MZTvmbGGp3uPaYM9PJXeLBo2hbxwFIfhlel7bQYzi', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('E20Awn5bBRudU2z060OGm4t0XoiMyDKp6ZaZp0lDZWwO8E0dqvfFexGrZxZhVrxl', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('6VvyqAH4HGCM52ei5Y72Uxc5mqmOKcroI1O0DFtUYJvUur7AS5USq6rsUi7LV6WY', '1209600', '2021-08-19', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('OihFszcFKD43iIfPCqCwDk1hnAB20Qyb34dVjLmSBicp8fjr8uHWsuDE0n6CGcXL', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('gGREeCbEvICWAgkuFLhYL24xLBg56flKcp6P4Mo0o3AKkOyAuzT1ZuGNWXleegGC', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('HE54mmGoGgQeDkWHEUhjddYDDpikSJJvAWCbuklrCEguFtEpkaM9Em0qaKu7kbbV', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('DpA6m0JT896cJhOnmQviaFVXaUhLhnX37ZevFUqbUNwvNzZVJUUxfN34apdYAnG4', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('sPfTj0WQPNI1TfQGLzqsJRYx6SF6WnMUPagBHUs8g9xhOlncDzccXRDMGKYlgTbM', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('fv2yVP9szXYkM58F25dCenz2XqvynNFtEajRHMEiX253WPIjk2MRvPbI9O1g6dsv', '1209600', '2021-08-20', 'ADMIN', NULL);
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`, `scopes`) VALUES
	('gMktex5H05ZWn6tjzksXNcdOWqwHhOZlrADbVWnXA71zPqk6D8vbl4PbCbtqWQDu', '1209600', '2021-08-23', 'ADMIN', NULL);
/*!40000 ALTER TABLE `accesstoken` ENABLE KEYS */;

-- Dumping structure for table kasirku.acl
CREATE TABLE IF NOT EXISTS `acl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property` varchar(255) DEFAULT NULL,
  `accessType` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `principalType` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `principalId` varchar(255) DEFAULT NULL,
  `menu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.acl: ~14 rows (approximately)
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(354, NULL, '*', 'ALLOW', 'ROLE', 'Wilayah', 'ADMIN', 'Wilayah');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(355, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Kabupaten / Kota');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(356, NULL, '*', 'ALLOW', 'ROLE', 'Menu', 'ADMIN', 'Menu');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(357, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Kecamatan');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(358, NULL, '*', 'ALLOW', 'ROLE', 'Province', 'ADMIN', 'Provinsi');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(359, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Chek');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(360, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Admin Menu');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(361, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Vendor');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(362, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Contract');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(363, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Cargo Order');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(364, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Master Parameter');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(365, NULL, '*', 'ALLOW', 'ROLE', 'Role', 'ADMIN', 'User');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(366, NULL, '*', 'ALLOW', 'ROLE', 'acl', 'ADMIN', 'Kelurahan');
INSERT INTO `acl` (`id`, `property`, `accessType`, `permission`, `principalType`, `model`, `principalId`, `menu`) VALUES
	(367, NULL, '*', 'ALLOW', 'ROLE', 'Customer', 'ADMIN', 'Customer');
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;

-- Dumping structure for table kasirku.lmss_mst_param
CREATE TABLE IF NOT EXISTS `lmss_mst_param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `GENERIC_TYPE` varchar(255) DEFAULT NULL,
  `LAST_UPDATE_BY` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.lmss_mst_param: ~24 rows (approximately)
/*!40000 ALTER TABLE `lmss_mst_param` DISABLE KEYS */;
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(1, 'C', 'Customer', 'subject_type', 'Admin', 'Active', '2021-07-30 10:47:34', '2021-07-30 10:47:36');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(2, 'V', 'Vendor', 'subject_type', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(3, 'Indi', 'I-Individual', 'tipe_cif', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(4, 'Corp', 'C-Corporate', 'tipe_cif', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(5, 'PT', 'Perusahaan Terbuka (PT)', 'jns_badan_usaha', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(6, 'CV', 'Commanditaire Venootschap (CV)', 'jns_badan_usaha', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(7, 'Persero', 'Persero', 'jns_badan_usaha', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(8, 'KTP', 'KTP', 'jns_id', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(9, 'SIM', 'SIM', 'jns_id', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(10, 'L', 'Laki-laki', 'jns_kelamin', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(11, 'P', 'Perempuan', 'jns_kelamin', 'Admin', 'Active', '2021-07-30 10:48:03', '2021-07-30 10:48:05');
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(12, 'NE', 'NonElektronik', 'tipe_barang', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(13, 'EL', 'Elektronik', 'tipe_barang', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(14, 'PH', 'Perhiasan', 'tipe_barang', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(15, 'Na', 'NonAsuransi', 'tipe_asuransi', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(16, 'AS', 'Asuransi', 'tipe_asuransi', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(17, 'IREG', 'IREGULER', 'order_cargo_service', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(18, 'REG', 'REGULER', 'order_cargo_service', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(19, 'TBA', 'Tipe Barang A', 'tipe_barang', NULL, 'Inactive', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(20, 'TBB', 'Tipe Barang B', 'tipe_barang', NULL, 'Inactive', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(21, 'TA1', 'Tipe Asuransi A', 'tipe_asuransi', NULL, 'Inactive', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(22, 'TA2', 'Tipe Asuransi B', 'tipe_asuransi', NULL, 'Inactive', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(23, 'JA1', 'Asuransi A', 'jenis_asuransi', NULL, 'Active', NULL, NULL);
INSERT INTO `lmss_mst_param` (`id`, `CODE`, `DESCRIPTION`, `GENERIC_TYPE`, `LAST_UPDATE_BY`, `STATUS`, `created_at`, `updated_at`) VALUES
	(24, 'JA2', 'Asuransi B', 'jenis_asuransi', NULL, 'Active', NULL, NULL);
/*!40000 ALTER TABLE `lmss_mst_param` ENABLE KEYS */;

-- Dumping structure for table kasirku.lmss_role
CREATE TABLE IF NOT EXISTS `lmss_role` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `LAST_UPDATE_BY` varchar(255) DEFAULT NULL,
  `ROLE_NAME` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `RIGHTS` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.lmss_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `lmss_role` DISABLE KEYS */;
INSERT INTO `lmss_role` (`ID`, `LAST_UPDATE_BY`, `ROLE_NAME`, `created`, `modified`, `RIGHTS`, `name`, `STATUS`, `description`) VALUES
	(1, 'ADMIN', 'ADMIN', '2021-07-29 14:41:00', '2021-08-16 08:55:52', NULL, 'Admin', 'Active', 'Admin');
/*!40000 ALTER TABLE `lmss_role` ENABLE KEYS */;

-- Dumping structure for table kasirku.lmss_t_menu
CREATE TABLE IF NOT EXISTS `lmss_t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.lmss_t_menu: ~16 rows (approximately)
/*!40000 ALTER TABLE `lmss_t_menu` DISABLE KEYS */;
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(1, 'customer', 'component/customer', 'Customer', 'fa-user', 0, 1);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(2, NULL, 'x', 'Admin Menu', 'fa-check-square', 0, 9);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(3, 'user', 'component/user', 'User', 'fa-user', 2, 1);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(4, 'Role', 'component/role', 'Role', 'fa-check-square', 2, 2);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(5, 'Menu', 'component/menu', 'Menu', 'fa-bars', 2, 3);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(6, NULL, 'x', 'Wilayah', 'fa-map-marker', 2, 4);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(7, NULL, 'component/province', 'Provinsi', 'fa-map-marker', 6, 1);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(8, NULL, 'component/regency', 'Kabupaten / Kota', 'fa-map-marker', 6, 2);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(9, NULL, 'component/district', 'Kecamatan', 'fa-map-marker', 6, 3);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(10, NULL, 'component/subdistrict', 'Kelurahan', 'fa-map-marker', 6, 4);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(11, NULL, 'omponent/master_parameter', 'Master Parameter', 'fa-database fas', 2, 5);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(15, NULL, 'component/vendor', 'Vendor', 'fa-check-square', 0, 2);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(16, NULL, 'component/branch', 'Cabang', 'fa-user', 2, 4);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(17, NULL, 'component/jobsheet', 'Cargo Order', 'fa-check-square', 0, 5);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(18, NULL, 'component/check', 'Chek', 'fa-check-square', 0, 5);
INSERT INTO `lmss_t_menu` (`id`, `model`, `url`, `label`, `icon`, `parentId`, `order`) VALUES
	(19, NULL, 'component/contract', 'Contract', 'fa-user', 0, 3);
/*!40000 ALTER TABLE `lmss_t_menu` ENABLE KEYS */;

-- Dumping structure for table kasirku.lmss_t_menu_model
CREATE TABLE IF NOT EXISTS `lmss_t_menu_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `accessType` varchar(255) DEFAULT NULL,
  `fk_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.lmss_t_menu_model: ~14 rows (approximately)
/*!40000 ALTER TABLE `lmss_t_menu_model` DISABLE KEYS */;
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(3, 'Role', NULL, '*', 3);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(4, 'Menu', NULL, '*', 5);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(6, 'Wilayah', NULL, '*', 6);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(7, 'Province', NULL, '*', 7);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(8, 'acl', 'ALLOW', '*', 8);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(9, 'acl', 'ALLOW', '*', 9);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(10, 'acl', 'ALLOW', '*', 10);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(24, 'Customer', 'ALLOW', '*', 1);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(27, 'acl', 'ALLOW', '*', 11);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(35, 'acl', 'ALLOW', '*', 19);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(36, 'acl', 'ALLOW', '*', 15);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(38, 'acl', 'ALLOW', '*', 18);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(39, 'acl', 'ALLOW', '*', 2);
INSERT INTO `lmss_t_menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(40, 'acl', 'ALLOW', '*', 17);
/*!40000 ALTER TABLE `lmss_t_menu_model` ENABLE KEYS */;

-- Dumping structure for table kasirku.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.menu: ~3 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `model`, `url`, `label`, `role_id`, `parentId`, `order`) VALUES
	(1, 'Customer', 'component/customer', 'Customer', NULL, NULL, NULL);
INSERT INTO `menu` (`id`, `model`, `url`, `label`, `role_id`, `parentId`, `order`) VALUES
	(2, 'Role', 'component/role', 'Role', NULL, NULL, NULL);
INSERT INTO `menu` (`id`, `model`, `url`, `label`, `role_id`, `parentId`, `order`) VALUES
	(3, 'user', 'component/user', 'User', NULL, NULL, NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table kasirku.mst_user
CREATE TABLE IF NOT EXISTS `mst_user` (
  `ID` varchar(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `realm` varchar(255) DEFAULT NULL,
  `emailVerified` varchar(255) DEFAULT NULL,
  `verificationToken` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.mst_user: ~3 rows (approximately)
/*!40000 ALTER TABLE `mst_user` DISABLE KEYS */;
INSERT INTO `mst_user` (`ID`, `username`, `email`, `phone`, `password`, `realm`, `emailVerified`, `verificationToken`, `name`) VALUES
	('ADMIN', 'admin', 'admin@cargo.com', '086662323', '$2a$10$3TtoW99jvGpiNwApjcYNKO/kdMpz14Pgfbg4sM4WHFvTLamYlqij.', '1', '1', NULL, 'ADMIN');
INSERT INTO `mst_user` (`ID`, `username`, `email`, `phone`, `password`, `realm`, `emailVerified`, `verificationToken`, `name`) VALUES
	('AHMAD1', 'ahamd1', 'ahmad1@gmail.com', NULL, NULL, NULL, NULL, NULL, 'Ahmad 1');
INSERT INTO `mst_user` (`ID`, `username`, `email`, `phone`, `password`, `realm`, `emailVerified`, `verificationToken`, `name`) VALUES
	('rashad', 'rashad', 'rashad@gmail.com', NULL, '$2a$10$.xwkH0rOjT1NY3HXPaJ8M.3pQQ.a1/H095LtuNQGbKkThhyOBzVqm', NULL, NULL, NULL, 'rashad');
/*!40000 ALTER TABLE `mst_user` ENABLE KEYS */;

-- Dumping structure for table kasirku.mst_user_role
CREATE TABLE IF NOT EXISTS `mst_user_role` (
  `IDF_USER` varchar(255) NOT NULL,
  `IDF_ROLE` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDF_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.mst_user_role: ~2 rows (approximately)
/*!40000 ALTER TABLE `mst_user_role` DISABLE KEYS */;
INSERT INTO `mst_user_role` (`IDF_USER`, `IDF_ROLE`) VALUES
	('ADMIN', 1);
INSERT INTO `mst_user_role` (`IDF_USER`, `IDF_ROLE`) VALUES
	('rashad', 1);
/*!40000 ALTER TABLE `mst_user_role` ENABLE KEYS */;

-- Dumping structure for table kasirku.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `stok` int(11) DEFAULT '0',
  `jml_terjual` int(11) DEFAULT '0',
  `fkJobsheet` int(11) DEFAULT NULL,
  `jenis_barang` varchar(255) DEFAULT NULL,
  `tgl_transaksi` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table kasirku.transaction: ~1 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(1, '1', 'Kopi', 100, 10, NULL, 'Konsumsi', '2021-05-01 13:56:38', '2024-02-08 13:56:40', '2024-02-08 08:42:04');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(2, NULL, 'Teh', 100, 19, NULL, 'Konsumsi', '2021-05-04 17:00:00', '2024-02-08 07:46:51', '2024-02-08 08:42:21');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(4, NULL, 'Kopi', 90, 15, NULL, 'Konsumsi', '2021-05-09 17:00:00', '2024-02-08 08:30:28', '2024-02-08 08:42:51');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(5, NULL, 'Pasta Gigi', 100, 20, NULL, 'Pembersih', '2021-05-10 17:00:00', '2024-02-08 08:43:28', '2024-02-08 08:43:28');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(6, NULL, 'Sabun Mandi', 100, 30, NULL, 'Pembersih', '2021-05-10 17:00:00', '2024-02-08 08:43:52', '2024-02-08 08:43:52');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(7, NULL, 'Sampo', 100, 25, NULL, 'Pembersih', '2021-05-11 17:00:00', '2024-02-08 08:44:17', '2024-02-08 08:44:17');
INSERT INTO `transaction` (`id`, `code`, `nama_barang`, `stok`, `jml_terjual`, `fkJobsheet`, `jenis_barang`, `tgl_transaksi`, `created_at`, `updated_at`) VALUES
	(8, NULL, 'Teh', 81, 5, NULL, 'Konsumsi', '2021-05-11 17:00:00', '2024-02-08 08:44:43', '2024-02-08 08:44:43');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
