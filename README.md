# Kasirku
Penjelasan Project
- Aplikasi ini menggunakan Framework Angular JS 
- Folder Project ini terdiri dari 2 project Back End dan Frond End
- Untuk Project Frond End ada di folder "client" menggunakan framework Angular Js
- Untuk Poject Back End ada di root project awal menggunakan framework Loopback versi 3
- Untuk  database menggunakan Mysql

Cara Menjalankan Aplikasi ini
-import database ada di folder "database"
-Masuk ke directory project jalankan perintah di cmd "node ." " atau "npm start"
-Maka project akan terbuka di http://localshot:3000
-interface API terbuka di http://localhost:3000/explorer
-Apabila terdapat error :
 1. di root folder project jalankan npm install
 2. masuk ke folder "client" jalan bower install
 3. kembali ke root project di CMD jalankan perintah "node ." atau "npm start"
 4. untuk setingan database ada di folder "service/datasource.json"  dan sql database di folder "database"
