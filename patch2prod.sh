#!/bin/sh

echo "sync scare-be to prod starting"
rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/ --exclude 'backup' --exclude 'docker' app@10.0.149.71:/home/app/scare-be/
# rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/server/ app@10.0.149.71:/home/app/scare-be/server/
# rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/client/app/ app@10.0.149.71:/home/app/scare-be/client/app/
echo "sync scare-be complete"
