#!/bin/sh

echo "sync scare-be starting"
rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/common/ app@10.16.5.94:/home/app/scare-be-dev/common/
rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/server/ app@10.16.5.94:/home/app/scare-be-dev/server/
rsync -urltv --delete -e ssh /users/hira/workspace/hrt/sc/scare-be/client/app/ app@10.16.5.94:/home/app/scare-be-dev/client/app/
echo "sync scare-be complete"
