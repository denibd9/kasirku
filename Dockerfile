FROM node:12.6
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm install pm2 -g
COPY . .
EXPOSE 443
CMD [ "pm2-runtime","--json", "ecosystem.config.js" ]
