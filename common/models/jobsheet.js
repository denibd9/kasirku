'use strict';

module.exports = function(Jobsheet) {
    const app = require('../../server/server');
	const axios = require("axios");
	//const axiosRetry = require('axios-retry');
	//const Jobsheet = app.models.Jobsheet
    Jobsheet.dijournalCargoStore =  (req, cb) => {
        
          axios.get("http://localhost/dijournal/api/v1/cargo/store", {params : req})
             .then(function(res){
                 console.log('cargoStore',res)
                 return  cb(null, res.data);
             })
             .catch(function(error, result){
                 return cb(error.response.data)
             });
     }
     
     Jobsheet.remoteMethod('dijournalCargoStore',{
       http: {verb: "post"},
       accepts: {arg: 'req', type: 'object',  'http' : {source : 'body'}},
       returns: {arg: 'result', type: 'object', 'root' : true }
     });
};
