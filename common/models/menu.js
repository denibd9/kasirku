
var app = require("../../server/server");
var _ = require('lodash');
module.exports = function(Menu) {
    Menu.remoteMethod('access',{
      accepts: {arg: 'userId', type: 'string'},
      returns: {arg: 'menus', type: 'array', 'root' : true},
      http: {path: '/access', verb: 'post'}
    });

    Menu.access = function (userId, callback) {
      var acl = app.models.acl;
      var menu;
      // console.log("userId : ", userId)
      acl.getAclByUserId(userId)
      .then(function(res){
        datas = res.filter(menu => menu.accessType == "*" || menu.accessType == "MENU"); 
        let models = _.uniq(datas.map(el => el.menu));
        Menu.find({where:{label:{inq: models}},order: 'order ASC'})
        .then((data) => {
          let menus = data.map((el) => {
            if (el.parentId == 0) {
              let d = el;
              d.subMenu = data.filter((el2) => {
                return el.id == el2.parentId;
              })
              d.activeMenu = false;
              return d;
            }
          }).filter((el) => {
            return el;
          })
          callback(null, menus);
        });
      })
      .catch(function(err){
        callback(err,null)
      })

      // var acl = app.models.acl;
      // let aclFilter = {
      //   where: {
      //     or : [
      //       {and : [{principalId: role},{accessType: "*"}]},
      //       {and : [{principalId: role},{accessType: "MENU"}]}          
      //     ]
      //       // principalId: role,
      //       // or: [{accessType: "*"}, {accessType: "MENU"}]
      //   }
      // };

      // acl.find(aclFilter).then((datas) => {
      //   let models = _.uniq(datas.map(el => el.menu));
      //   Menu.find({where:{label:{inq: models}},order: 'order ASC'})
      //   .then((data) => {
      //     let menus = data.map((el) => {
      //       if (el.parentId == 0) {
      //         let d = el;
      //         d.subMenu = data.filter((el2) => {
      //           return el.id == el2.parentId;
      //         })
      //         d.activeMenu = false;
      //         return d;
      //       }
      //     }).filter((el) => {
      //       return el;
      //     })
      //     callback(null, menus);
      //   });
      // });
    };

    Menu.getUserMenu = async function (user_id){
        var acl = app.models.Acl;
        var user_role = app.models.MstUserRole;
        var role = app.models.Role;
        var roles = [];
        var menus = [];
        var listOfMenu = [];

        await user_role.find({where : {IDF_USER : user_id}})
        .then(function(res){
            res.forEach(el =>{
              roles.push(el.IDF_ROLE)
            })
        }) 

        await role.find({ where : { ID : { inq : roles}}})
        .then(function (res){
           roles = [];
           res.forEach(el =>{
            roles.push(el.ROLE_NAME)
          })
        })

        await acl.find( { 
          where : { 
            and : [
              {principalId : { inq : roles}},
              { or : [
                {accessType : "*"},
                {accessType : "Menu"}
              ]}
            ]
          } 
        })
        .then(function(res){
          
          console.log('resAcl1', res)
          for(let el of res){
            menus.push(el.menu);
          }
          menus = [...new Set(menus)];
        })

        await Menu.find({where : { label : {inq : menus}}})
        .then(function(res){
            listOfMenu = res.filter(el =>  el.parentId == 0);
            menus = res.filter(el => el.parentId != 0);
        })      

        while(menus.length > 0){
          let parent = getParentFromForest(listOfMenu, menus[0].parentId);
          if(!parent)
            parent = getParentFromForest(menus, menus[0].parentId); 
          if(parent){
            if(!parent.subMenu){
              parent.subMenu = [];
            }
            parent.activeMenu = false;
            parent.subMenu.push(menus[0]);
            parent.subMenu = parent.subMenu.sort(
              (a,b) => {
                return a.order - b.order;
              }
            )
          }
          menus = menus.filter(el => el.id != menus[0].id);
     
        }
        return listOfMenu;
    }

    function getParentFromForest(forest, id){
      let result = null;
      for(let el of forest){
        if(el.id == id)   return el;
        if(el.subMenu)    result = getParentFromTree(el, id);
        if(result)        return result
      }
      return result
    }

    function getParentFromTree(tree,id){
      let result = null;
      if(tree.id == id)
          return tree;
      if(tree.subMenu){
          for(let el of tree.subMenu){
            if(el.id == id)   return el;
            if(el.subMenu)    result =  getParentFromForest(el.subMenu, id)
            if(result)        return result
          }
      }      
      return null;
    }
  
    Menu.remoteMethod('getUserMenu',{
        accepts:{arg: 'user_id', type: 'string',required : true, description : "Id of user"},
        returns: {arg: 'menus', type: 'array', 'root' : true},
        http: {path: '/getUserMenu', verb: 'get',source : 'body'}
    });
};
