'use strict';
module.exports = function(MasterGeneric) {
	// generic_type
	MasterGeneric.remoteMethod ('listParameterType',{
		http : {path : '/listParameterType', verb : 'get'},
		accepts : {arg : 'queryParam' , type : 'string'},
		description: 'Get data with query',
		returns: { arg: 'data', type: ['MasterGeneric'], root: true }
	});
	// generic_type_name
	MasterGeneric.remoteMethod ('listParameterTypeName',{
		http : {path : '/listParameterTypeName', verb : 'get'},
		accepts : {arg : 'queryParam' , type : 'string'},
		description: 'Get data with query for lmss_mst_param type name',
		returns: { arg: 'data', type: ['MasterGeneric'], root: true }
	});

	// generic_type 
	MasterGeneric.listParameterType = function (queryParam,cb){
		var ds = MasterGeneric.dataSource;
		var query = "SELECT DISTINCT GENERIC_TYPE FROM lmss_mst_param";
		queryParam = query; 
		ds.connector.query(query, function (err,result){
            if (err) console.error(err);
            //console.log("hasil",result);
            cb(err, result);
		});
	}
	// generic_type name 
	MasterGeneric.listParameterTypeName = function (queryParam,cb){
		var ds = MasterGeneric.dataSource;
		var query = "SELECT DISTINCT GENERIC_TYPE_NAME, GENERIC_TYPE FROM lmss_mst_param "+
			"WHERE "+ 
			"GENERIC_TYPE = 'commercial_kk3pilar_faktor '"+
			"OR GENERIC_TYPE = 'commercial_kk3pilar_komponen' "+
			"OR GENERIC_TYPE = 'commercial_kk3pilar' "+
			"ORDER BY GENERIC_TYPE_NAME ASC"; 
		queryParam = query;
		ds.connector.query(query, function (err,result){
			//console.log("query:", query, result); 
            if (err) console.error(err);
            cb(err, result);
		});
	}


	/*SLA 1: Pengumpulan dan penyerahan data s/d Verifikasi Data permohonan pembiayaan */ 
	MasterGeneric.updateParameterKerjasama = function (queryParam,cb){
		var ds = MasterGeneric.dataSource;
		var query =	"UPDATE p3 "+
			"set p3.STATUS = x.STATUS "+
			"	FROM( "+
			"		SELECT p1.ID, p1.GENERIC_TYPE, p1.DESCRIPTION, p1.STATUS as OLD_STATUS, p1.date1, p1.date2, "+
			"		CASE "+
			"		WHEN p1.date2 < CONVERT(DATE, GETDATE()) THEN 'I' "+
			"		WHEN p1.date2 >= CONVERT(DATE, GETDATE()) THEN 'A' "+
			"		ELSE p1.STATUS "+  
			"		END as STATUS "+
			"		FROM lmss_mst_param p1 "+
			"			INNER JOIN lmss_mst_param p2 on p2.ID = p1.ID "+
			"		WHERE p1.GENERIC_TYPE IN('commercial_notaris', 'commercial_prshn_asuransi','commercial_external_appraisal') "+
			"	)x "+
			"inner join lmss_mst_param p3 on p3.ID = x.ID";
 
		queryParam = query; 
		ds.connector.query(query, function (err,result){
            if (err) console.error(err);
            //checkStatus(result);
			cb(err, result);
		});
	}

	MasterGeneric.remoteMethod ('updateParameterKerjasama',{	
		http : {path : '/updateParameterKerjasama', verb : 'get'},
		accepts : {arg : 'queryParam' , type : 'string'},
		description: 'update data with query',
		returns: { arg: 'data', type: ['MasterGeneric'], root: true }
	});
	/*----------------------------------------------------------*/
};
