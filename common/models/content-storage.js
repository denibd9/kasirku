'use strict';


module.exports = function(Contentstorage) {
    var app = require('../../server/server');
    var fs = require("fs")
    Contentstorage.remoteMethod ( "ContentImageUpload", {
            http : {
                path : '/ContentImageUpload', verb : "POST"},
            accepts : [
                {arg: 'req', type: 'object', 'http': {source: 'req'}},
                {arg: 'res', type: 'object', 'http': {source: 'res'}},
            ],
            returns : [
                {arg : 'result', type : 'object'},
                {arg : 'link',  type : 'string'}
            ]
        }
    )

    Contentstorage.ContentImageUpload = function (req, res, cb){
        var result = {}
        var link;

        Contentstorage.upload(req,res,{container: "img"},
            function onSuccess (c, response) {
                result = response;
                if (c != null){
                    result = "No file content uploaded";
                    link = "no file";

                    // console.log("no file detected", response)
                    cb(c,result,link);
                }else{
                    link = app.get('apiserver')+"/api/contentStorages/img/download/"+response.files.image[0].name;
                    cb(null,result,link);
                }
        },
            function onFailed (c,response) {
                result = null;
                link = null;

                // console.log("failed, response", response)
                cb(null,result,link);
        })
    }

    Contentstorage.remoteMethod ( "ContentVideoUpload", {
        http : {
            path : '/ContentVideoUpload', verb : "POST"},
        accepts : [
            {arg: 'req', type: 'object', 'http': {source: 'req'}},
            {arg: 'res', type: 'object', 'http': {source: 'res'}},
        ],
        returns : [
            {arg : 'result', type : 'object'},
            {arg : 'link',  type : 'string'}
          ]
        }
    )

    Contentstorage.ContentVideoUpload = function (req, res, cb){
        var result = {}
        var link;

        Contentstorage.upload(req,res,{container: "video"},
            function onSuccess (c, response) {
                result = response;
                if (c != null){
                    result = "No file content uploaded";
                    link = "no file";
                    cb(c,result,link);
                }else{
                    link = app.get('apiserver')+"/api/contentStorages/video/download/"+response.files.video[0].name;
                    cb(null,result,link);
                }
        },
            function onFailed (c,response) {
                result = null;
                link = null;
                cb(null,result,link);
        })
    }

    Contentstorage.remoteMethod ( "applicationUpload", {
        http : {
            path : '/applicationUpload', verb : "POST"},
        accepts : [
            {arg: 'req', type: 'object', 'http': {source: 'req'}},
            {arg: 'res', type: 'object', 'http': {source: 'res'}},
        ],
        returns : [
            {arg : 'result', type : 'object'},
            {arg : 'link',  type : 'string'}
          ]
        }
    )

    Contentstorage.applicationUpload = function (req, res, cb){
        var result = {}
        var link;
        Contentstorage.upload(req,res,{container: "application"},
            function onSuccess (c, response) {
                result = response;
                if (c != null){
                    result = "No file content uploaded";
                    link = "no file";
                    cb(c,result,link);
                }else{
                    console.log(response)
                    link = app.get('apiserver')+"/api/contentStorages/application/download/"+response.files.file[0].name;
                    cb(null,result,link);
                }
            },
            function onFailed (c,response) {
                result = null;
                link = null;
                cb(null,result,link);
            }
        )
    }
};
