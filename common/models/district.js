'use strict';

module.exports = function(District) {
  District.importKecamatan = (req, cb) => {
    const csv = require('csv-parser');
    const fs = require('fs');
    const results = [];
    console.log('proses');

    fs.createReadStream('C:\\FEEDCOLL\\ZONASI_KECAMATAN.csv')
      .pipe(csv())
      .on('data', (data) => {
        //if (data.FID_PROVINCE == 11 && data.CODE_KOTA_KABUPATEN >= 100 && data.CODE_KOTA_KABUPATEN < 150) {//1
        //if(data.CODE_KOTA_KABUPATEN >= 100 && data.CODE_KOTA_KABUPATEN < 150) {//1
        //if(data.CODE_KOTA_KABUPATEN >= 150 && data.CODE_KOTA_KABUPATEN < 200) {//2
        //if(data.CODE_KOTA_KABUPATEN >= 200 && data.CODE_KOTA_KABUPATEN < 250) {//3
        //if(data.CODE_KOTA_KABUPATEN >= 250 && data.CODE_KOTA_KABUPATEN < 275) {//4
        //if(data.CODE_KOTA_KABUPATEN >= 275 && data.CODE_KOTA_KABUPATEN < 300) {//5
        //if(data.CODE_KOTA_KABUPATEN >= 300 && data.CODE_KOTA_KABUPATEN < 325) {//6
        //if(data.CODE_KOTA_KABUPATEN >= 325 && data.CODE_KOTA_KABUPATEN < 350) {//7
        //if(data.CODE_KOTA_KABUPATEN >= 350 && data.CODE_KOTA_KABUPATEN < 400) {//8
        //if(data.CODE_KOTA_KABUPATEN >= 400 && data.CODE_KOTA_KABUPATEN < 450) {//9
        //if(data.CODE_KOTA_KABUPATEN >= 450 && data.CODE_KOTA_KABUPATEN < 500) {//10
       // if(data.CODE_KOTA_KABUPATEN >= 500 && data.CODE_KOTA_KABUPATEN < 550) {//11
       // if(data.CODE_KOTA_KABUPATEN >= 550 && data.CODE_KOTA_KABUPATEN < 600) {//12
        //if(data.CODE_KOTA_KABUPATEN >= 600 && data.CODE_KOTA_KABUPATEN < 650) {//13
        if(data.CODE_KOTA_KABUPATEN >= 650 && data.CODE_KOTA_KABUPATEN < 700) {//14
          results.push(data);
        }
      })
      .on('end', () => {
        results.forEach((r)=>{
          const ds = District.dataSource;
          const query =	`UPDATE mst_district
              SET CODE_KECAMATAN_ZONASI = ${r.CODE_KECAMATAN_ZONASI}, fk_zonasi = ${r.fk_zonasi}
              WHERE ID = ${r.FID_DISTRICT}`;
          // queryParam = query;
          ds.connector.query(query, function(err, result) {
            if (err) console.error('err', err);
            // checkStatus(result);
            // cb(err, result);
          });
        });
        cb(null, {count: results.length, results: results});
      });
  };
  District.remoteMethod('importKecamatan', {
    http: {verb: 'post'},
    accepts: {arg: 'req', type: 'object', 'http': {source: 'body'}},
    returns: {arg: 'result', type: 'object', 'root': true},
  });
};
