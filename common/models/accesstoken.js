'use strict';
var app = require('../../server/server');
module.exports = function(AccessToken) {

  AccessToken.observe('before save', function(ctx, next) {
    AccessToken.destroyAll({userId: ctx.instance.userId}, function (err, result) {
      console.log('new login', ctx.instance.id)
      if (result.count > 0) {
        console.log('destroyAll the same session', ctx.instance.userId)
      }
      if (!ctx.instance || ctx.instance.id) {
        // We are running a partial update or the instance already has an id
        return next();
      }
    });
  });

  AccessToken.resolve   = function(id, cb) {
    this.findById(id, function(err, token) {
      if (err) {
        cb(err);
      } else if (token) {
        token.validate(function(err, isValid) {
          if (err) {
            cb(err);
          } else if (isValid) {
            cb(null, token);
          } else {
            var e = new Error('Invalid Access Token');
            e.status = e.statusCode = 401;
            e.code = 'INVALID_TOKEN';
            cb(e);
          }
        });
      } else {
        var e = new Error('Invalid Access Token');
        e.status = e.statusCode = 401;
        e.code = 'INVALID_TOKEN';
        cb(e);
      }
    });
  };
};
