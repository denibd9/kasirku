'use strict';

module.exports = function(Acl) {
    var app = require('../../server/server')
    Acl.remoteMethod('getAclByUserId',{
        http : {path : '/getAclByUserId', verb : 'post'},
        accepts : {arg : 'userId' , type : 'string', required : true},
        returns: { arg: 'res', type: 'array', root: true }
    });
  
    Acl.getAclByUserId = async function(userId){
    var acl = app.models.Acl;
    var user = app.models.User;
    var roleId; 
    var result;
    var error;
    await user.findOne({where : {id : userId}})
    .then(function(res){
        roleId = res.fk_role;
    })
    .catch(function(err){
        return err
    })

    await acl.find({where : { principalId : roleId }})
    .then(function(res){
        result = res;
    })
    .catch(function(err){
        error =  err;
    })

    if(result)
        return result
    else   
        return error
    }

    // Acl.beforeRemote('**',async function checkAccess(ctx,unused, next) {
    //     var acl = app.models.Acl;
	// 	var method = ctx.req.method;
		
	// 	var error = new Error("You must login");
	// 	error.statusCode = 400;
 
    //     if(ctx.req.accessToken){
    //         if(method == "POST" || method == "PUT" || method == "PATCH"){
    //             method = "WRITE"
    //         }
    
    //         if(method == "GET"){
    //             return
    //         }

    //         error = new Error("You don't have access acl model");
    //         var userId = ctx.req.accessToken.userId;
    //         var model = "acl";
            
    //         await acl.getAclByUserId(userId)
    //         .then(function(res){
    //             let available = res.find(
    //                 data => (data.model == model) && (data.accessType == "*" || data.accessType == method)
    //             )
    //             if(available)
    //                 return;
    //             else{
    //                 return next(error)
    //             }
    //         })	
    //         .catch(function(err){
    //             return next(err)
    //         })
    //     }else   
    //         return next(error);

    // });
};
