'use strict';

module.exports = function(Param) {
  var pubsub = require('../../server/pubsub.js');
  var app = require('../../server/server');
  Param.validatesUniquenessOf('code', {message: 'code is not unique'});
  var froalaText = '<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>'

  // Param.beforeRemote('**',async function checkAccess(ctx,unused, next) {
  //     var acl = app.models.Acl;
  //     var method = ctx.req.method;

  //     var error = new Error("You must login");
  //     error.statusCode = 400;

  //     if(ctx.req.accessToken){
  //         if(method == "POST" || method == "PUT" || method == "PATCH"){
  //             method = "WRITE"
  //         }

  //         if(method == "GET"){
  //             return
  //         }

  //         error = new Error("You don't have access menu model");
  //         var userId = ctx.req.accessToken.userId;
  //         var model = "param";

  //         await acl.getAclByUserId(userId)
  //         .then(function(res){
  //             let available = res.find(
  //                 data => (data.model == model) && (data.accessType == "*" || data.accessType == method)
  //             )
  //             if(available)
  //                 return;
  //             else{
  //                 return next(error)
  //             }
  //         })
  //         .catch(function(err){
  //             return next(err)
  //         })
  //     }else
  //         return next(error);

  // });

  Param.observe("before save",async function createHtmlFile(ctx,next){
    const fs = require("fs")
    var remark;
    var oldFileName;
    var filename;
    console.log('simpan param baru', JSON.stringify(ctx.isNewInstance), JSON.stringify(ctx.instance))
    if(ctx.isNewInstance){
      console.log("create new data")
      if(ctx.instance.remark){
        remark = JSON.parse(JSON.stringify( ctx.instance.remark));
        filename = ctx.instance.title + Math.random().toString().substr(2) + ".html";
        ctx.instance.filename =  filename;
        remark = remark;
        remark = remark.replace(froalaText,"")
        ctx.instance.remark = remark
        // delete ctx.instance["remark"];
        await fs.writeFile("content/html/"+filename, remark, function (err) {
          if (err) return err;
        });
      }
      // delete ctx.instance.unsetAttribute("remark");
    }else{
      console.log("update data",ctx.currentInstance)
      if(ctx.currentInstance.remark && ctx.currentInstance.filename){
        console.log("ok")
        console.log("filename", ctx.currentInstance.filename)
        remark = JSON.parse(JSON.stringify( ctx.currentInstance.remark));
        console.log("remark", remark);
        oldFileName = JSON.parse(JSON.stringify(ctx.currentInstance.filename))
        filename = oldFileName;
        ctx.currentInstance.filename =  filename;
        remark = remark;
        remark = remark.replace(froalaText,"")
        ctx.currentInstance.remark = remark
        // delete ctx.currentInstance["remark"];
        await fs.unlink("remark/html/"+oldFileName, function (err) {
          if (err) return err;
          console.log('File deleted! 1');
        });

        await fs.writeFile("content/html/"+filename, remark, function (err) {
          if (err) return err;
          console.log('File created! 2');

        });
      }
      console.log("update data image ",ctx.currentInstance.image)
      if(ctx.currentInstance.image != null){
        console.log("delete iamge")
        let image = JSON.parse(JSON.stringify(ctx.currentInstance.image))
        await fs.unlink("content/img/"+image, function (err) {
          if (err) return err;
          console.log('Image deleted!');
        });
      }
    }
    return;
  })

  Param.observe('after save', function (ctx, next) {

    var socket = Param.app.io;
    if(ctx.isNewInstance){
      pubsub.publish(socket, {
        collectionName : 'Param',
        data: ctx.instance,
        method: 'POST'
      });
    }else{
      pubsub.publish(socket, {
        collectionName : 'Param',
        data: ctx.instance,
        modelId: ctx.instance.code,
        method: 'PUT'
      });
      if (ctx.instance.code = 'alert_participant') {
        app.alert_participant = ctx.instance.params;
      }else if (ctx.instance.code = 'dispenser_threshold_limit') {
        app.SIMDISPENSER_THRESHOLD = ctx.instance.name;
      }
    }
    console.log(`after save param, ${JSON.stringify(ctx.instance)}`)
    next();
    // return;
  });

  function setFilter(filter){
    let data = {};
    data.name = "";
    data.code = "";
    data.type = "";
    data.limit = 0;
    data.skip = 0;
    if(filter)
    data = Object.assign(data,JSON.parse(filter));
    return data;
  }
  Param.validatesUniquenessOf('code',{message : "code is not unique"})

  Param.getParamsValue = function (filter, cb){
    let params = [];
    let result = [];
    filter = setFilter(filter);

    Param.find({}, function onSuccess(err,res){
      if(err)
      cb(err,null)

      res.forEach(element => {
        element.params.forEach(row => {
          let data = {};
          data = Object({},row)
          data.id = element.id;
          data.type = element.name;
          data.code = row.code;
          data.name = row.name;
          params.push(data);
        })
      });

      // if(filter.name != "")
      //     params = params.filter(el => el.name == filter.name);

      // if(filter.code != "")
      //     params = params.filter(el => el.code == filter.code);

      // if(filter.type != "")
      //     params = params.filter(el => el.type == filter.type);
      params = params.filter(el =>
        el.code.includes(filter.code)
        &&  el.name.includes(filter.name)
        &&  el.type.includes(filter.type)
        );

        if(filter.limit == 0)
        filter.limit = params.length;
        if(filter.limit > params.length)
        filter.limit = params.length;

        result = params.slice(filter.skip,filter.limit)

        cb(null,result);

      })
    }

    Param.remoteMethod('getParamsValue',{
      accepts:{arg: 'filter', type: 'string', description : "JSON filter parameter are code,name,type,skip and limit"},
      returns: {arg: 'params', type: 'array', 'root' : true},
      http: {path: '/getParamsValue', verb: 'get',source : 'body'}
    });
  };
