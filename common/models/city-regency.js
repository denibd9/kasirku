'use strict';

module.exports = function(Cityregency) {
  Cityregency.importKotaKabupaten = (req, cb) => {
    const csv = require('csv-parser');
    const fs = require('fs');
    const results = [];
    console.log('proses');
    cb(null, {});
    fs.createReadStream('C:\\FEEDCOLL\\ZONASI_KOTA_KABUPATEN.csv')
      .pipe(csv())
      .on('data', (data) => {
        results.push(data);
      })
      .on('end', () => {
        results.forEach((r)=>{
          const ds = Cityregency.dataSource;
          const query =	`UPDATE mst_city_regency
          SET CODE_KOTA_KABUPATEN_ZONASI = ${r.CODE_KOTA_KABUPATEN}
          WHERE ID = ${r.FID_CITY_REGENCY}`;
          // queryParam = query;
          ds.connector.query(query, function(err, result) {
            if (err) console.error('err', err);
            // checkStatus(result);
            // cb(err, result);
          });
        });
      });
  };

  Cityregency.remoteMethod('importKotaKabupaten', {
    http: {verb: 'post'},
    accepts: {arg: 'req', type: 'object', 'http': {source: 'body'}},
    returns: {arg: 'result', type: 'object', 'root': true},
  });
};
