'use strict';

module.exports = function(User) {
  var app = require('../../server/server');
  User.validatesUniquenessOf('mdn', {message: 'mdn is not unique'});

  function setUserAttribtue(data) {
    let user = {};
    user.name = data.name;
    user.password = data.password;
    user.email = data.email;
    user.fk_role = data.fk_role;
    user.fullname = data.fullname;
    user.username = data.username;
    if (data.id)
    user.id = data.id;
    return user;
  }



  User.remoteMethod("loginV2", {
    accepts: { arg: "req", type: "object", http: { source: "body" } },
    returns: { arg: "loginV2", type: "object", root: true },
    http: { path: "/loginV2", verb: "post" },
  });

  User.loginV2 = async function (req) {
    let User = app.models.user;
    let AccessToken = app.models.AccessToken;
    let returnObj = {};

    try {
      let result = await User.findOne({ where: { username: req.username, shaPassword: req.password } });
      if (!result) {
        return { resultCode: 0, resultMsg: 'Username or password is missing.' }
      }
      returnObj.user = JSON.parse(JSON.stringify(result));
      let token = await AccessToken.create({ ttl: 1209600, userId: result.id });
      if (!token) {
        return {};
      }
      returnObj.token = token;
      delete returnObj.user.shaPassword;
      delete returnObj.user.salt;
      delete returnObj.user.saltIterations;
      delete returnObj.user.saltHash;
      delete returnObj.user.password;
      delete returnObj.user.saltCode;
      console.log("user logged in", JSON.stringify(returnObj));
      return returnObj;
    } catch (err) {
      console.log('got error??', err)
      return err;
    };
  };

};
