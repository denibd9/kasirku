module.exports = {
  apps: [{
    script: "server/server.js",
    watch: ["server", "client"],
    // Delay between restart
    watch_delay: 1000,
    instances  : 1,
    exec_mode  : "cluster",
    ignore_watch : ["node_modules", "client/img"],
    watch_options: {
      "followSymlinks": false
    },
    "env": {
        "NODE_ENV": "development",
        "TZ": "Asia/Bangkok"
    },
    "env_production": {
        "NODE_ENV": "production",
        "TZ": "Asia/Bangkok"
    },
    "log_date_format" : "MM-DD HH:mm:ss.SSS Z"
  }]
}

