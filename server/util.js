var moment = require('moment');

module.exports.dateDiff = function dateDiff(date1, date2){
    var a = moment(date1, 'DD/MM/YYYYhh:mm:ss');
    var b = moment(date2, 'DD/MM/YYYYhh:mm:ss');
    return a.diff(b,'days')+1;
}

module.exports.notifMsgParsing = function notifMsgParsing(msg, obj){
    var m;
    var re = /{([a-zA-Z]+)}/g;
    do {
      m = re.exec(msg);
      if (m) {
          msg = msg.replace(m[0],obj[m[1]]);
      }
    } while (m);
    var re = /{`currency\(([a-zA-Z]+)\)}/g;
    do {
        m = re.exec(msg);
        if (m) {
            msg = msg.replace(m[0],parseInt(obj[m[1]])/100);
        }
    } while (m);
    var re = /{`date\(([a-zA-Z]+)\)}/g;
    do {
        m = re.exec(msg);
        if (m) {
            msg = msg.replace(m[0],moment(obj[m[1]]).format('DD-MM-YYYY'));
        }
    } while (m);
    re = /{`dateDiff\(([a-zA-Z]+),([a-zA-Z]+)\)}/g;
    do {
        m = re.exec(msg);
        if (m) {
            msg = msg.replace(m[0],this.dateDiff(obj[m[1]],obj[m[2]]));
        }
    } while (m);
    return msg;
}
