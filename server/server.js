'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
const io = require('socket.io-client');
var https = require('https');
var sslConfig = require('./ssl-config');
var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

app.httpsStart = function() {
  const options = {
    key: sslConfig.privateKey,
    cert: sslConfig.certificate,
  };
  let httpserver = https.createServer(options, app);
  httpserver.listen(app.get('https_port'), function() {
    const baseUrl = app.get('apiserver');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  return httpserver;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  console.log('err',err)
  if (err) throw err;

  // start the server if `$ node server.js`
  // console.log("\n\n run \n\n ");
  if (require.main === module) {
    // app.start();
    // app.httpsStart();
  }
});
