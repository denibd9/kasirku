module.exports = function(app) {
  var uuidv1 = require("uuid/v1");
  let LATEST_APP = null
  let CONNECTED_SOCKS = [];
  let socketID = null;

  let listener = function (socket) {
    var kioskCode = socket.handshake.headers.kioskcode;
    var deviceLog = {};
    deviceLog["status"] = "connected";
    deviceLog["device"] = "fe-socket";
    deviceLog["galleryCode"] = "q";
    deviceLog["regionCode"] = "q";
    deviceLog["issuer"] = "gateway";
    var vmcode = kioskCode
    deviceLog["kioskCode"] = kioskCode||vmcode;
    console.log('req headers', JSON.stringify(socket.handshake.headers))
    let clientid = socket.handshake.headers['x-clientid'] ? socket.handshake.headers['x-clientid'] : kioskCode;
    let clientname = socket.handshake.headers['x-client-name']
    let clientType = kioskCode ? "GW" : socket.handshake.headers['user-agent'].indexOf('scare-fe') >= 0 ? "VM-FE" : socket.handshake.headers['x-clientid'] ? "VM-FE-Service[" + socket.handshake.headers['x-client-name'] + "]" : "Web";
    console.log('new client handshake', socket.id, kioskCode, clientType, clientid, clientname)
    let clientCode = clientid + '_' + clientname;
    if (clientid && clientname) {
      // app.redisc.set(clientCode, socket.id);
      // console.log('storing new sockId to redis', clientCode, socket.id)
    }
    if (vmcode) {
      socketID = socket.id;
      app.models.device_log.create(deviceLog);
      var kiosk = null
      app.models.kiosk
      .findOne({ where: { code: kioskCode }, include: "kiosk_gallery" })
      .then(function (res) {
        if (res) {
          kiosk = res;
          if (!LATEST_APP) {
            app.models.application.findOne({ where: { status: 'latest' } })
            .then(function (latestapp) {
              LATEST_APP = latestapp;
              kiosk.latestApp = LATEST_APP;
              console.log('latest: ', LATEST_APP)
              socket.emit('connected/'+kioskCode, kiosk)
            })
          } else {
            kiosk.latestApp = LATEST_APP;
            console.log('latest: ', LATEST_APP)
            socket.emit('connected/'+kioskCode, kiosk)
          }
        }

      });

      socket.on('createDeviceLog', async function(datas){
        console.log('device log', JSON.stringify(datas));
        if (typeof datas[Symbol.iterator] !== 'function') datas = [datas]
        for(data of datas){
          data.logid = uuidv1();
          data.date = Date.now();
          let messages = [];
          if(data.device=="dispenser"){
            if(data.remark.card_position){
              if(data.remark.tray_status=="No card in tray")
              messages.push(data.remark.tray_status)
              if(data.remark.bin_status=="Error card bin full")
              messages.push(data.remark.bin_status)
              data.alert={
                kioskCode:data.kioskCode,
                device:data.device,
                messages:messages
              }
              data.alertStatus = "not resolved";
              socket.emit('newAlert',data);
              app.fireAlert(data.alert);
            }
          }
          if(data.device=="printer"){
            if(data.remark.other_status!="-"){
              data.alert={
                kioskCode:data.kioskCode,
                device:data.device,
                messages:[data.remark.other_status]
              }
              data.alertStatus = "not resolved";
            }else{
              data.alert={
                kioskCode:data.kioskCode,
                device:data.device,
                messages:["Printer Ready to Use"]
              }
              data.alertStatus="resolved";
            }
            data.remark = data.remark.temp;
            socket.emit("newAlert", data);
            app.fireAlert(data.alert);
          }

          await app.models.device_log.create(data);


        }
      });
    }
    socket.on('createActivityLog', function(data){
      data.logid = uuidv1();
      data.date = Date.now();
      delete data.remark.payment_methods;
      delete data.remark.category;
      console.log('new activity',JSON.stringify(data));
      app.models.activity_log.create(data);
    });
    socket.on('createTransactionLog', async function(data){
      if(!data.logid) data.logid = uuidv1();
      data.date = Date.now();
      delete data.remark.denoms;
      delete data.remark.payment_methods;
      delete data.remark.package_category;
      delete data.remark.photo;
      console.log('new Trx', JSON.stringify(data));
      try {
        let res = await app.models.subscriber.findOne({ where: { mdn: data.mdn } });
        if (res) {
          data.iccid = res.iccid;
          data.iccid_status = true;
          data.imsi = res.imsi;
        }
        let stock = await app.models.stock.findOne({ where: { code: data.mdn } });
        data.card_type = (data.mdn.indexOf('62881') > -1 || data.mdn.indexOf('62882') > -1) ? 'ST' : "SF";
        console.log('create trx log & subsc found')
        // app.models.transaction_log.create(data);
        if (data.alert) {
          socket.emit('newAlert', data);
          app.fireAlert(data.alert);
        }
      } catch (error) {
        console.log('create trx log & subsc not found')
        // app.models.transaction_log.create(data);
      }

    });
    socket.on('transactionResultAcknowledgement', async function(data){
      data.date = Date.now();
      console.log('Trx result',JSON.stringify(data));
      transactionThatDispenseSIMCard = ["postpaid", "prepaid", "card-replacement"]
      if(transactionThatDispenseSIMCard.indexOf(data.transaction_type)>-1 && data.remark.iccid_status=="true"){
        let stockMovement = {
          movement_type:"kioskToGallerySalesout", qty:1, remark:{transactionId:data.logid},
          status:"Success", stocks:[], iccid:data.remark.iccid
        }
        await app.models.stock.findOne({where:{code:data.mdn}})
        .then(function(res){
          if(res){
            res.qty = res.qty-1;
            app.models.stock.upsert(res,function(err, obj){
            });
            stockMovement.stocks.push(res);
          }
        }).catch(function(err){
          console.log(err);
        })
        app.models.stock_movement.create(stockMovement);
      }
      // app.models.transaction_log.create(data);
    });
    kioskCode = kioskCode || socket.handshake.headers['x-clientid'];
    socket.on('ping/'+kioskCode, function(){
      console.log('got ping from ', kioskCode)
      socket.emit('ping/'+kioskCode, "pong sent");
    });
    socket.on('update-apps', function(app){
      LATEST_APP = app;
    });

    socket.on('disconnect', function(reason){
      deviceLog["status"] = "disconnected";
      if (kioskCode) {
        console.log(kioskCode+` Is Killed because of Disconnect +++++++++++++++++++++++++++++++++++++++++++++++++++++++++` + reason);
        // if(data != null){
        //     console.log(" Data Not Found ")
        // }else{
        // app.models.device_log.create(data);
        // }
      }
      console.log('client socket disconnected', kioskCode, reason, socket.id);
    });
    console.log('connected wss client', JSON.stringify(Object.keys(app.io.engine.clients)));
    console.log('connected ws client', JSON.stringify(Object.keys(app.ioHttps.engine.clients)));
    if (clientType == 'Web') {
      socket.emit('authenticated','Berhasil authenticated')
    }
  };

  app.io = require('socket.io')(app.httpsStart(), {pingInterval: 50000, pingTimeout: 10000});
  // const redis = require("socket.io-redis");
  // console.log('connecting socket to redis', app.get('redis.host') + ":" + app.get('redis.port'));
  // app.io.adapter(redis({host: app.get('redis.host'), port: app.get('redis.port')}));
  app.io.origins('*:*');
  app.io.on('connection', listener);
  app.ioHttps = require('socket.io')(app.start());
  // app.ioHttps.use(app.monitorio({ port: 8001 }));
  app.ioHttps.origins('*:*');
  app.ioHttps.on('connection', listener);

  const withTimeout = (onSuccess, onTimeout, timeout) => {
    let called = false;

    const timer = setTimeout(() => {
      if (called) return;
      called = true;
      onTimeout();
    }, timeout);

    return (...args) => {
      if (called) return;
      called = true;
      clearTimeout(timer);
      onSuccess.apply(this, args);
    }
  }

  app.io.sendMsg2Kiosk = function sendMsg2Kiosk(eventName, msg, ack) {
    console.log('send msg to https ', eventName, msg)
    if (msg) {
      // this.emit(eventName, msg, ack)
      this.emit(eventName, msg, ack, withTimeout(() => {
        console.log(`ack callback ${eventName} success!`);
      }, () => {
        console.log(`ack callback ${eventName} timeout!`);
        throw {resultCode: 2, resultMsg: 'server didnt get the response from '+msg.kioskCode}
      }, 15000));
    } else {
      this.emit(eventName)
    }
  }
  app.sendMsg2Kiosk = (eventName, msg, ack, onfailed) => {
    let sock = {};
    if (ack && eventName.indexOf('payment-result') == 0) {
      // app.redisc.get(msg.kioskCode + "_FE-Service")
      // .then(async sockId => {
      //   console.log('send message to client ', msg.kioskCode + "_FE-Service", sockId, msg)
      //   try {
      //     if (sockId) {
      //       let socket = app.io.sockets.connected[sockId];
      //       if (socket) {
      //         console.log('send event to VM '+msg.kioskCode + `(${socket.id})`)
      //         socket.emit(eventName, msg, ack);
      //         /* socket.emit(eventName, msg, ack, withTimeout((msg, ack) => {
      //           console.log(`ack callback ${eventName} success! ${msg} - ${ack}`);
      //         }, (er, dt) => {
      //           console.log(`ack callback ${eventName} timeout! ${er} - ${dt}`);
      //           onfailed({resultCode: 2, resultMsg: 'server didnt get the response from '+msg.kioskCode})
      //         }, 1*60*1000)); */
      //       } else {
      //         console.log('server couldnot reach the VM')
      //         onfailed({resultCode: 1, resultMsg: `VM-${msg.kioskCode} not connected`})
      //       }
      //     }
      //   } catch (error) {
      //     onfailed(error)
      //   }
      // })
      return;
    } else {
      console.log('send msg to (all) ', eventName, msg)
      if (msg) {
        app.io.emit(eventName, msg, ack)
        app.ioHttps.emit(eventName, msg, ack)
      } else {
        app.io.emit(eventName)
        app.ioHttps.emit(eventName)
      }
    }
  }
  // const sockClient = app.io.connect(app.get('apiserver'), { timeout: 60000, extraHeaders: { 'x-clientid': 'BE-CLIENT', 'x-client-name': 'BE-Service' } });
};
