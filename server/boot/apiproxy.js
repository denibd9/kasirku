'use strict';

module.exports = (app) =>{
  let moment = require('moment');
  let crypto = require('crypto');
  const fs = require('fs');
  let path = require('path');
  const privateKey = fs.readFileSync(path.join(__dirname, '../private/payment.key'), 'utf-8');

  app.signingPayload = function(req) {
    const signer = crypto.createSign('sha256');
    signer.update(JSON.stringify(req));
    signer.end();
    const signature = signer.sign({key: privateKey, passphrase: 'Smartfren2021'});
    const buff = new Buffer.from(signature);
    const signatureB64 = buff.toString('base64');
    console.log('Digital Signature: ' + signatureB64);
    return signatureB64;
  };

  app.paymentAggregator = function(params) {
    let data = [];
    return {
      request: function request(par) {
      },
      confirm: function confirm(par) {
      },
      data: data,
    };
  };

  app.authBearer = function authBearer(version = '', isPayAgg = false) {

    var date = moment(new Date(), 'MM-DD-YYYY').utc().format('ddd, DD MMM YYYY HH:mm:ss UTC');
    var hmac, keyID;
    var server = isPayAgg ? 'payagg' : 'apiproxy';
    // console.log('Auth key to TYK: ', server, keyID, version, app.get('' + server)['secretKey' + version]);
    hmac = crypto.createHmac('sha256', app.get('' + server)['secretKey' + version]);
    keyID = app.get('' + server)['keyID' + version];
    console.log('Auth key to TYK post: ', server, keyID, version);
    hmac.update(`date: ${date}`);
    let signature = encodeURIComponent(hmac.digest('base64'));

    let headers = {
      'Date': date,
      'Authorization': `Signature KeyId="${keyID}",algorithm="hmac-sha256",signature="${signature}"`,
      'Content-Type': 'application/json',
    };
    console.log('headers', server, JSON.stringify(headers), date);
    return headers;
  };

  /**
  * --header 'Authorization: Signature keyid="5d1d334b00d0463f7934d511eb51617fa4314d19be0ba0ba9912b9d4",algorithm="hmac-sha256",signature="1oUix8hk97B5GVcnD94zGo2OXmt0xqSAGsnXo%2Fw%2Fi%2B8%3D"' \
  * --header 'Date: Wed, 16 Sep 2020 02:44:19 UTC' \
  */
  app.authpayagg = function authpayagg() {
    var date = moment(new Date(), 'MM-DD-YYYY').utc().format('ddd, DD MMM YYYY HH:mm:ss UTC');
    var hmac = crypto.createHmac('sha256', app.get('payagg').secretKey);
    hmac.update(`date: ${date}`);
    let signature = encodeURIComponent(hmac.digest('base64'));

    let headers = {
      'Date': date,
      'Authorization': `Signature KeyId="${app.get('payagg').keyID}",algorithm="hmac-sha256",signature="${signature}"`,
      'Content-Type': 'application/json',
    };
    return {
      headers: headers,
      signature: signature,
    };
  };

  app.genMqmToken = function genMqmToken(mdn, timestamp) {
    let key = app.get('mqm').key
    return crypto.createHash('sha256').update(`${key}${mdn}${timestamp}`).digest('hex');
  };
};
