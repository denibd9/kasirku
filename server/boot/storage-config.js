module.exports = function(app) {
    app.dataSources.contentStorage.connector.getFilename = function(uploadingFile, req, res) {
        let ext = uploadingFile.name.substr(uploadingFile.name.lastIndexOf('.')+1)
        return Math.random().toString().substr(2) + "." + ext;
      };
  };